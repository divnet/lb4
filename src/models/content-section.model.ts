import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { SectionPage } from './section-page.model';
import { DataContentString, DataContentStringRelations } from './data-content-string.model';
import { DataContentFile, DataContentFileRelations } from './data-content-file.model';

@model({
  settings: {
    strictObjectIDCoercion: true,
  }
})
export class ContentSection extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  publicName: string;

  @property({
    type: 'string',
  })
  baseParent: string;

  @property({
    type: 'boolean',
  })
  isBase: boolean;

  @property({
    type: 'number'
  })
  order: number;

  @belongsTo(() => SectionPage)
  sectionPageId: string;

  @hasMany(() => DataContentString)
  dataContentStrings: DataContentString[];

  @hasMany(() => DataContentFile)
  dataContentFiles: DataContentFile[];

  constructor(data?: Partial<ContentSection>) {
    super(data);
  }
}

export interface ContentSectionRelations {
  // describe navigational properties here
  dataContentStrings?: DataContentStringRelations,
  dataContentFiles?: DataContentFileRelations
}

export type ContentSectionWithRelations = ContentSection & ContentSectionRelations;

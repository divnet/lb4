import { Entity, model, property } from '@loopback/repository';

@model({ settings: { strict: false } })
export class Language extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  ISO_code: string;

  @property({
    type: 'string',
  })
  ISO_codeName: string;

  @property({
    type: 'string',
  })
  nativeName: string;

  @property({
    type: 'boolean',
    default: true,
  })
  active: boolean;

  @property({
    type: 'string',
  })
  scope?: string;

  @property({
    type: 'string',
  })
  type?: string;

  @property({
    type: 'boolean',
    default: 0,
  })
  isDefault: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Language>) {
    super(data);
  }
}

export interface LanguageRelations {
  // describe navigational properties here
}

export type LanguageWithRelations = Language & LanguageRelations;

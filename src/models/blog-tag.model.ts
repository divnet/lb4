import { Entity, model, property } from '@loopback/repository';

@model({ settings: { strict: false } })
export class BlogTag extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<BlogTag>) {
    super(data);
  }
}

export interface BlogTagRelations {
  // describe navigational properties here
}

export type BlogTagWithRelations = BlogTag & BlogTagRelations;

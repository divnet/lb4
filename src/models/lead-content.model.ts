import {Entity, model, property} from '@loopback/repository';

@model()
export class LeadContent extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  created_at?: string;

  @property({
    type: 'string',
  })
  updated_at?: string;

  @property({
    type: 'string',
  })
  nombre?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
  })
  mensaje?: string;

  @property({
    type: 'string',
  })
  datetime?: string;

  @property({
    type: 'string',
  })
  origen?: string;

  @property({
    type: 'string',
  })
  platformName?: string;

  @property({
    type: 'string',
  })
  platformVersion?: string;

  @property({
    type: 'string',
  })
  platformProduct?: string;

  @property({
    type: 'string',
  })
  platformOsArchitecture?: string;

  @property({
    type: 'string',
  })
  platformOsFamily?: string;

  @property({
    type: 'string',
  })
  platformOsVersion?: string;

  @property({
    type: 'string',
  })
  platformIp?: string;

  @property({
    type: 'string',
  })
  platformCity?: string;

  @property({
    type: 'string',
  })
  platformRegion?: string;


  constructor(data?: Partial<LeadContent>) {
    super(data);
  }
}

export interface LeadContentRelations {
  // describe navigational properties here
}

export type LeadContentWithRelations = LeadContent & LeadContentRelations;

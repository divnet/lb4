import {Entity, model, property} from '@loopback/repository';

@model()
export class MetaTags extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  publicName?: string;

  @property({
    type: 'string',
  })
  content?: string;

  @property({
    type: 'boolean',
  })
  isProperty?: boolean;

  @property({
    type: 'string',
  })
  urImage?: string;


  constructor(data?: Partial<MetaTags>) {
    super(data);
  }
}

export interface MetaTagsRelations {
  // describe navigational properties here
}

export type MetaTagsWithRelations = MetaTags & MetaTagsRelations;

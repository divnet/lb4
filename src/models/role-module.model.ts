import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Module } from './module.model';
import { SysUserRole } from './sys-user-role.model';

@model()
export class RoleModule extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'boolean',
  })
  create: boolean;

  @property({
    type: 'boolean',
  })
  read: boolean;

  @property({
    type: 'boolean',
  })
  update: boolean;

  @property({
    type: 'boolean',
  })
  del: boolean;

  @belongsTo(() => Module)
  moduleId: string;

  @belongsTo(() => SysUserRole)
  SysUserRoleId: string;

  constructor(data?: Partial<RoleModule>) {
    super(data);
  }
}

export interface RoleModuleRelations {
  // describe navigational properties here
}

export type RoleModuleWithRelations = RoleModule & RoleModuleRelations;

import { Entity, model, property, belongsTo } from '@loopback/repository';
import { ContentSection } from './content-section.model';

@model({
  settings: {
    strictObjectIDCoercion: true,
  }
})
export class DataContentFile extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'object',
  })
  file: object;

  @property({
    type: 'object',
  })
  extraData: object;

  @property({
    type: 'string',
  })
  type: string

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  publicName: string;

  @property({
    type: 'number',
  })
  order: number;

  @belongsTo(() => ContentSection)
  contentSectionId: string;

  constructor(data?: Partial<DataContentFile>) {
    super(data);
  }
}

export interface DataContentFileRelations {
  // describe navigational properties here
}

export type DataContentFileWithRelations = DataContentFile & DataContentFileRelations;

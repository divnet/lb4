import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { ContentSection } from './content-section.model';

@model({
  settings: {
    strictObjectIDCoercion: true,
  }
})
export class DataContentString extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  type: string

  @property({
    type: 'string',
  })
  content: string;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  publicName?: string;

  @property({
    type: 'object'
  })
  extraData: object;

  @property({
    type: 'number',
  })
  order: number;

  @belongsTo(() => ContentSection)
  contentSectionId: string;

  constructor(data?: Partial<DataContentString>) {
    super(data);
  }
}

export interface DataContentStringRelations {
  // describe navigational properties here
}

export type DataContentStringWithRelations = DataContentString & DataContentStringRelations;

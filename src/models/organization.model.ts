import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { Page, PageWithRelations } from './page.model';
import { User } from './user.model';
import { SysUserRole } from './sys-user-role.model';

@model({ settings: { strictObjectIDCoercion: true } })
export class Organization extends Entity {
  @property({
    type: 'string',
    id: true,
    mongodb: {
      dataType: 'ObjectID'
    },
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name: string;
  @property({
    type: 'boolean',
    default: true,
  })
  active: boolean;
  @property({
    type: 'boolean',
    default: true,
  })
  canUpdate: boolean;

  @property({
    type: 'string',
  })
  description?: string;

  @hasMany(() => Page, { keyTo: 'organizationId' })
  pages: Page[];

  @hasMany(() => User)
  users: User[];

  @hasMany(() => SysUserRole)
  sysUserRoles: SysUserRole[];




  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Organization>) {
    super(data);
  }
}

export interface OrganizationRelations {
  // describe navigational properties here
  pages?: PageWithRelations
}

export type OrganizationWithRelations = Organization & OrganizationRelations;

import { belongsTo, Entity, model, property } from '@loopback/repository'

import { SysUserRole } from './sys-user-role.model'
import { User } from './user.model'

@model()
export class SysUserData extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @belongsTo(() => User)
  userId: string;

  @belongsTo(() => SysUserRole)
  sysUserRoleId: string;

  constructor(data?: Partial<SysUserData>) {
    super(data);
  }
}

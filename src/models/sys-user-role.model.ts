import { Entity, model, property, belongsTo, hasOne } from '@loopback/repository';
import { SysUserData } from './sys-user-data.model';
import { RoleModule } from './role-module.model';

@model()
export class SysUserRole extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name: string

  @property({
    type: 'string',
  })
  description: string

  @property({
    type: 'number',
  })
  accessLevel: number;

  @belongsTo(() => SysUserData)
  userId: string;

  @belongsTo(() => RoleModule)
  roleModuleId: string;

  @hasOne(() => SysUserData)
  sysUserData: SysUserData;

  @property({
    type: 'string',
  })
  organizationId?: string;

  constructor(data?: Partial<SysUserRole>) {
    super(data);
  }
}

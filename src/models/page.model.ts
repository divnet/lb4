import { Entity, model, property, hasMany, belongsTo } from '@loopback/repository';
import { SectionPage, SectionPageRelations } from './section-page.model';
import { Organization, OrganizationWithRelations } from './organization.model';

@model({
  settings: {
    strictObjectIDCoercion: true,
  }
})
export class Page extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  publicName: string;

  @property({
    type: 'boolean',
  })
  active: boolean;

  @property({
    type: 'string',
  })
  menuTitle: string;

  @property({
    type: 'string'
  })
  urlOnWebPage: string;

  @hasMany(() => SectionPage)
  sectionPages: SectionPage[];

  @belongsTo(() => Organization, { name: 'organization' })
  organizationId: string;

  constructor(data?: Partial<Page>) {
    super(data);
  }
}

export interface PageRelations {
  // describe navigational properties here
  sectionPages?: SectionPageRelations,
  organization?: OrganizationWithRelations

}

export type PageWithRelations = Page & PageRelations;

import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { Page } from './page.model';
import { ContentSection } from './content-section.model';

@model({
  settings: {
    strictObjectIDCoercion: true,
  }
})
export class SectionPage extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  publicName: string;

  @property({
    type: 'number'
  })
  order: number;

  @belongsTo(() => Page)
  pageId: string;

  @hasMany(() => ContentSection)
  contentSections: ContentSection[];

  constructor(data?: Partial<SectionPage>) {
    super(data);
  }
}

export interface SectionPageRelations {
  // describe navigational properties here
}

export type SectionPageWithRelations = SectionPage & SectionPageRelations;

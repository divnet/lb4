import { belongsTo, Entity, model, property } from '@loopback/repository'

import { User } from './user.model'

@model()
export class AppUserData extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<AppUserData>) {
    super(data);
  }
}

import { Entity, model, property } from '@loopback/repository';

@model({ settings: { strict: false } })
export class BlogEntry extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  title: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  body?: string;

  @property({
    type: 'object',
  })
  mainImg?: object;

  @property({
    type: 'object',
  })
  thumbnail?: object;

  @property({
    type: 'date',
  })
  publishDate: string;

  @property({
    type: 'string',
  })
  author?: string;

  @property({
    type: 'number',
    default: 0,
  })
  likes?: number;

  @property({
    type: 'boolean',
    default: false,
  })
  featured?: boolean;

  @property({
    type: 'string',
  })
  url?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<BlogEntry>) {
    super(data);
  }
}

export interface BlogEntryRelations {
  // describe navigational properties here
}

export type BlogEntryWithRelations = BlogEntry & BlogEntryRelations;

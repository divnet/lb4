import { belongsTo, Entity, hasMany, hasOne, model, property } from '@loopback/repository'

import { AppUserData } from './app-user-data.model'
import { Log } from './log.model'
import { SysUserData } from './sys-user-data.model'
import { UserCredentials } from './user-credentials.model'

@model({settings: {strict: false}})
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  username: string;

  @property({
    type: 'string',
  })
  firstName?: string;

  @property({
    type: 'string',
  })
  token: string;

  @property({
    type: 'date',
  })
  token_expiration: string;

  @property({
    type: 'string',
  })
  lastName: string;

  @property({
    type: 'string',
  })
  avatar: string;

  @property({
    type: 'boolean',
  })
  firstTime: boolean;

  @property({
    type: 'string',
  })
  email: string;

  @property({
    type: 'string',
  })
  password: string;

  @belongsTo(() => SysUserData)
  sysUserDataId: string;

  @belongsTo(() => AppUserData)
  appUserDataId: string;

  @property({
    type: 'string',
  })
  organizationId?: string;

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials;

  @hasMany(() => Log)
  logs: Log[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;

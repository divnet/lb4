import { AuthenticationComponent } from '@loopback/authentication'
import { JWTAuthenticationComponent } from '@loopback/authentication-jwt'
import { BootMixin } from '@loopback/boot'
import { ApplicationConfig } from '@loopback/core'
import { RepositoryMixin, SchemaMigrationOptions } from '@loopback/repository'
import { RestApplication } from '@loopback/rest'
import { RestExplorerBindings, RestExplorerComponent } from '@loopback/rest-explorer'
import { ServiceMixin } from '@loopback/service-proxy'
import multer from 'multer'
import path from 'path'

import {
  EmailServiceBindings,
  FILE_UPLOAD_SERVICE,
  PasswordHasherBindings,
  STORAGE_DIRECTORY,
  TokenServiceBindings,
  TokenServiceConstants,
  UserServiceBindings,
} from './keys'
import { OrganizationRepository, UserRepository } from './repositories'
import { UserCredentialsRepository } from './repositories/user-credentials.repository'
import { MySequence } from './sequence'
import { MailerService } from './services/email'
import { BcryptHasher } from './services/hash.password.encrypt'
import { JWTService } from './services/jwt-service'
import { MyUserService } from './services/user-service'

// Seeds data
// File Upload
// Auth
export class Lb4Application extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    options = Object.assign(
      {},
      {
        rest: {
          port: 3000,
          basePath: '/api',
          openApiSpec: {
            servers: [{url: 'http://127.0.0.1:3000'}],
            setServersFromRequest: true,
          },
          protocol: 'https',
        },
        security: [
          {
            api_key: ['api_key'],
          },
        ],
        components: {
          securitySchemes: {
            api_key: {
              type: 'apiKey',
              name: 'api_key',
              in: 'header',
            },
          },
        },
      },
      options,
    );

    super(options);
    // Set up the custom sequence
    this.sequence(MySequence);
    //set up binding
    this.setupBinding();
    //this.seedData()
    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.bind(RestExplorerBindings.CONFIG).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);
    this.configureFileUpload(options.fileStorageDirectory);
    this.projectRoot = __dirname;

    this.configureFileUpload();

    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };

    // ------ ADD SNIPPET AT THE BOTTOM ---------
    // Mount authentication system
    this.component(AuthenticationComponent);
    // Mount jwt component
    this.component(JWTAuthenticationComponent);
  }
  async migrateSchema(options?: SchemaMigrationOptions) {
    // 1. Run migration scripts provided by connectors
    await super.migrateSchema(options);
    // 2. Make further changes. When creating predefined model instances,
    // handle the case when these instances already exist.
    const organizationRepo = await this.getRepository(OrganizationRepository);
    const UserRepo = await this.getRepository(UserRepository);
  }
  /**
   * Configure `multer` options for file upload
   */
  protected configureFileUpload(destination?: string) {
    // Upload files to `dist/.sandbox` by default
    destination = destination ?? path.join(__dirname, '../.sandbox');
    this.bind(STORAGE_DIRECTORY).to(destination);
    const multerOptions: multer.Options = {
      storage: multer.diskStorage({
        destination,
        // Use the original file name as is
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
    };
    // Configure the file upload service with multer options
    this.configure(FILE_UPLOAD_SERVICE).to(multerOptions);
  }

  setupBinding(): void {
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService);
    this.bind(UserServiceBindings.USER_REPOSITORY).toClass(UserRepository);
    this.bind(UserServiceBindings.USER_CREDENTIALS_REPOSITORY).toClass(
      UserCredentialsRepository,
    );
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      TokenServiceConstants.TOKEN_SECRET_VALUE,
    );
    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
      TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE,
    );
    this.bind(EmailServiceBindings.EMAIL_SERVICE).toClass(MailerService);
  }
}

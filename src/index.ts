import { ApplicationConfig } from '@loopback/core'

import { Lb4Application } from './application'

export {Lb4Application};

export async function main(options: ApplicationConfig = {}) {
  const app = new Lb4Application(options);
  await app.boot();
  await app.migrateSchema();
  app.basePath('/api');
  await app.start();
  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);
  return app;
}

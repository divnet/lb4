export const roleSeeds = [
    {   
        name: 'AdminGOD', 
        description: 'Rol GOD',
        // organizationId: organization[0].__data.id, 
        value: 0, 
        namePublic: 'GOD'
    },
    {   
        name: 'Admin', 
        description: 'Rol Admin',
        // organizationId: organization[1].__data.id, 
        value: 1,
        namePublic: 'Administrador'
    },
];
export const moduleSeeds = [
    {
      name: "Organizaciones",
      description: "Módulo de Organizaciones"
    },
    {
      name: "Usuarios",
      description: "Módulo de Usuarios"
    },
    {
      name: "Roles",
      description: "Módulo de Roles"
    },
    {
      name: "Módulos",
      description: "Módulo de Usuarios"
    },
    {
      name: "Web",
      description: "Módulo de Web"
    },
    {
      name: "Privilegios de usuarios",
      description: "Módulo de Privilegios de usuarios"
    },
    {
      name: "Blog",
      description: "Modulo de Blog"
    }
];
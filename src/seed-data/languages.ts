export const languageSeeds = [
    {
        ISOcode: "bs",
        ISOcodeName: "Bosnian",
        scope: "Individual",
        type: "Living",
        nativeName: "bosanski; босански",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "ca",
        ISOcodeName: "Catalan; Valencian",
        scope: "Individual",
        type: "Living",
        nativeName: "català",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "cs",
        ISOcodeName: "Czech",
        scope: "Individual",
        type: "Living",
        nativeName: "čeština; český jazyk",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "cy",
        ISOcodeName: "Welsh",
        scope: "Individual",
        type: "Living",
        nativeName: "Cymraeg; y Gymraeg",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "da",
        ISOcodeName: "Danish",
        scope: "Individual",
        type: "Living",
        nativeName: "dansk",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "de",
        ISOcodeName: "German",
        scope: "Individual",
        type: "Living",
        nativeName: "Deutsch",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "el",
        ISOcodeName: "Greek, Modern (1453–)",
        scope: "Individual",
        type: "Living",
        nativeName: "Νέα Ελληνικά",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "en",
        ISOcodeName: "English",
        scope: "Individual",
        type: "Living",
        nativeName: "English",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "es",
        ISOcodeName: "Spanish; Castilian",
        scope: "Individual",
        type: "Living",
        nativeName: "español; castellano",
        active: true,
        isDefault: true
    },
    {
        ISOcode: "fa",
        ISOcodeName: "Persian",
        scope: "Macrolanguage",
        type: "Living",
        nativeName: "فارسی",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "fo",
        ISOcodeName: "Faroese",
        scope: "Individual",
        type: "Living",
        nativeName: "føroyskt",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "fr",
        ISOcodeName: "French",
        scope: "Individual",
        type: "Living",
        nativeName: "français",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "ga",
        ISOcodeName: "Irish",
        scope: "Individual",
        type: "Living",
        nativeName: "Gaeilge",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "he",
        ISOcodeName: "Hebrew",
        scope: "Individual",
        type: "Living",
        nativeName: "עברית",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "hi",
        ISOcodeName: "Hindi",
        scope: "Individual",
        type: "Living",
        nativeName: "हिन्दी",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "hr",
        ISOcodeName: "Croatian",
        scope: "Individual",
        type: "Living",
        nativeName: "hrvatski",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "ht",
        ISOcodeName: "Haitian; Haitian Creole",
        scope: "Individual",
        type: "Living",
        nativeName: "kreyòl ayisyen",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "hu",
        ISOcodeName: "Hungarian",
        scope: "Individual",
        type: "Living",
        nativeName: "magyar nyelv",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "id",
        ISOcodeName: "Indonesian",
        scope: "Individual",
        type: "Living",
        nativeName: "bahasa Indonesia",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "is",
        ISOcodeName: "Icelandic",
        scope: "Individual",
        type: "Living",
        nativeName: "íslenska",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "it",
        ISOcodeName: "Italian",
        scope: "Individual",
        type: "Living",
        nativeName: "italiano; lingua italiana",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "ja",
        ISOcodeName: "Japanese",
        scope: "Individual",
        type: "Living",
        nativeName: "日本語",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "jv",
        ISOcodeName: "Javanese",
        scope: "Individual",
        type: "Living",
        nativeName: "ꦧꦱꦗꦮ",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "ka",
        ISOcodeName: "Georgian",
        scope: "Individual",
        type: "Living",
        nativeName: "ქართული",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "ko",
        ISOcodeName: "Korean",
        scope: "Individual",
        type: "Living",
        nativeName: "한국어",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "la",
        ISOcodeName: "Latin",
        scope: "Individual",
        type: "Ancient",
        nativeName: "Lingua latīna",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "lb",
        ISOcodeName: "Luxembourgish; Letzeburgesch",
        scope: "Individual",
        type: "Living",
        nativeName: "Lëtzebuergesch",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "lg",
        ISOcodeName: "Ganda",
        scope: "Individual",
        type: "Living",
        nativeName: "Luganda",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "mn",
        ISOcodeName: "Mongolian",
        scope: "Macrolanguage",
        type: "Living",
        nativeName: "монгол хэл; ᠮᠣᠩᠭᠣᠯ ᠬᠡᠯᠡ",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "nl",
        ISOcodeName: "Dutch; Flemish",
        scope: "Individual",
        type: "Living",
        nativeName: "Nederlands; Vlaams",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "no",
        ISOcodeName: "Norwegian",
        scope: "Macrolanguage",
        type: "Living",
        nativeName: "norsk",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "pl",
        ISOcodeName: "Polish",
        scope: "Individual",
        type: "Living",
        nativeName: "Język polski",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "pt",
        ISOcodeName: "Portuguese",
        scope: "Individual",
        type: "Living",
        nativeName: "português",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "ro",
        ISOcodeName: "Romanian; Moldavian; Moldovan",
        scope: "Individual",
        type: "Living",
        nativeName: "limba română",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "ru",
        ISOcodeName: "Russian",
        scope: "Individual",
        type: "Living",
        nativeName: "русский язык",
        active: true,
        isDefault: false
    },
    {
        ISOcode: "sk",
        ISOcodeName: "Slovak",
        scope: "Individual",
        type: "Living",
        nativeName: "slovenčina; slovenský jazyk",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "sl",
        ISOcodeName: "Slovenian",
        scope: "Individual",
        type: "Living",
        nativeName: "slovenski jezik; slovenščina",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "sr",
        ISOcodeName: "Serbian",
        scope: "Individual",
        type: "Living",
        nativeName: "српски / srpski",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "sv",
        ISOcodeName: "Swedish",
        scope: "Individual",
        type: "Living",
        nativeName: "svenska",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "vi",
        ISOcodeName: "Vietnamese",
        scope: "Individual",
        type: "Living",
        nativeName: "Tiếng Việt",
        active: false,
        isDefault: false
    },
    {
        ISOcode: "zh",
        ISOcodeName: "Chinese",
        scope: "Macrolanguage",
        type: "Living",
        nativeName: "中文; 汉语; 漢語",
        active: true,
        isDefault: false
    }
];
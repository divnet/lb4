import { Application, CoreBindings, inject, LifeCycleObserver, lifeCycleObserver } from '@loopback/core'
import { repository } from '@loopback/repository'

import { PasswordHasherBindings } from '../keys'
import {
  ModuleRepository,
  OrganizationRepository,
  RoleRepository,
  SysUserDataRepository,
  SysUserRoleRepository,
  UserRepository,
} from '../repositories'
import { BcryptHasher } from '../services/hash.password.encrypt'

/**
 * This class will be bound to the application as a `LifeCycleObserver` during
 * `boot`
 */
@lifeCycleObserver('')
export class InitialCreatorObserver implements LifeCycleObserver {
  constructor(
    @inject(CoreBindings.APPLICATION_INSTANCE) private app: Application,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(RoleRepository)
    public roleRepository: RoleRepository,
    @repository(OrganizationRepository)
    public organizationRepo: OrganizationRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public hasher: BcryptHasher,
    @repository(SysUserRoleRepository)
    public sysRole: SysUserRoleRepository,
    @repository(SysUserDataRepository)
    public dataUser: SysUserDataRepository,
    @repository(ModuleRepository)
    public moduleRepo: ModuleRepository,
  ) {}

  /**
   * This method will be invoked when the application starts
   */

  async start(): Promise<void> {
    // Add your logic for start
    await this.organizations();
    await this.roles();
    await this.users();
    await this.matcher();
    await this.orgMatcher();
    await this.matcher2();
    await this.generateModules();
    // await this.matcher()
  }

  async organizations(): Promise<Object> {
    const orgFinder = await this.organizationRepo.find();

    if (Object.keys(orgFinder).length === 0) {
      const _organizations = [
        {
          name: 'Guaostudio GOD',
          description: 'Organizacion de Guaostudio GOD',
          active: true,
        },
        {
          name: 'ORG Cliente',
          description: 'Organizacion de Cliente',
          active: true,
        },
      ];
      _organizations.forEach((element) => {
        this.organizationRepo.create(element);
      });
      return {message: 'ORG CREADONAS'};
    } else {
      return {message: 'ORG ya echeemstem'};
    }
  }

  async roles(): Promise<Object> {
    const clear = await this.sysRole.find();
    //console.log("-----" + clear);
    if (Object.keys(clear).length === 0) {
      const _rol1 = {
        name: 'ADMIN GOD',
        description: 'ROL GOD',
        accessLevel: 0,
      };

      const _rol2 = {
        name: 'ADMIN',
        description: 'ADMIN',
        accessLevel: 1,
      };
      //console.log(finder);
      await this.sysRole.create(_rol1);
      await this.sysRole.create(_rol2);
      const message = {Message: 'Roles creados con èxito'};
      console.log(message);
      return message;
    } else {
      const message = {Message: 'Ya existen los roles'};
      console.log(message);
      return message;
    }
  }

  async users(): Promise<Object> {
    const clear = await this.userRepository.find();
    if (Object.keys(clear).length === 0) {
      const pass = await this.hasher.hashPassword('Guao2017.-');
      const passClient = await this.hasher.hashPassword('Cliente2020.-');

      const adminGuao = {
        username: 'Admin',
        firstName: 'Guaostudio',
        lastName: 'GOD',
        email: 'admin@guaostudio.com',
        password: pass,
      };
      const client = {
        username: 'cliente',
        firstName: 'ORG',
        lastName: 'CLIENTE',
        email: 'cliente@guaostudio.com',
        password: passClient,
      };

      const savedAdmin = await this.userRepository.create(adminGuao);
      await this.userRepository
        .userCredentials(savedAdmin.id)
        .create({password: adminGuao.password});
      const savedClient = await this.userRepository.create(client);
      await this.userRepository
        .userCredentials(savedClient.id)
        .create({password: client.password});

      const message = {Message: 'Usuario(s) creado con èxito'};
      console.log(message);
      return message;
    } else {
      const message = {Message: 'Ya existe el usuario admin'};
      console.log(message);
      return message;
    }
  }
  async matcher(): Promise<Object> {
    const ifEmpty = await this.dataUser.find();
    if (Object.keys(ifEmpty).length === 0) {
      const sourceId = await this.userRepository.findOne({
        where: {email: 'admin@guaostudio.com'},
      });
      const sourceId2 = await this.sysRole.findOne({
        where: {name: 'ADMIN GOD'},
      });
      if (sourceId && sourceId2) {
        await this.dataUser.create({
          userId: sourceId.id,
          sysUserRoleId: sourceId2.id,
        });
      }
      const sourceId1 = await this.userRepository.findOne({
        where: {email: 'cliente@guaostudio.com'},
      });
      const sourceId12 = await this.sysRole.findOne({where: {name: 'ADMIN'}});
      if (sourceId1 && sourceId12) {
        await this.dataUser.create({
          userId: sourceId1.id,
          sysUserRoleId: sourceId12.id,
        });
      }
    }
    const Message = {Message: 'Update'};
    return Message;
  }
  async orgMatcher(): Promise<Object> {
    const orgIdUno = await this.organizationRepo.findOne({
      where: {name: 'Guaostudio GOD'},
    });
    const idUser = await this.userRepository.findOne({
      where: {email: 'admin@guaostudio.com'},
    });

    if (orgIdUno && idUser) {
      const uno = {
        organizationId: orgIdUno.id,
      };
      await this.userRepository.updateById(idUser.id, uno);
    }

    return {message: 1};
  }
  async matcher2(): Promise<Object> {
    const orgIdDos = await this.organizationRepo.findOne({
      where: {name: 'ORG Cliente'},
    });
    const idUserDos = await this.userRepository.findOne({
      where: {email: 'cliente@guaostudio.com'},
    });
    if (orgIdDos && idUserDos) {
      const dos = {
        organizationId: orgIdDos.id,
      };
      await this.userRepository.updateById(idUserDos.id, dos);
      //console.log("dos");
    }
    return {message: 2};
  }

  async generateModules(): Promise<Object> {
    const findModules = await this.moduleRepo.find();
    if (Object.keys(findModules).length === 0) {
      const modules = [
        {
          name: 'Organizaciones',
          description: 'Modulo de Organizaciones',
        },
        {
          name: 'Usuarios',
          description: 'Modulo de Usuarios',
        },
        {
          name: 'Roles',
          description: 'Modulo de Roles',
        },
        {
          name: 'Web',
          description: 'Modulo de Web',
        },
        {
          name: 'Modulos',
          description: 'Modulo de Modulos',
        },
        {
          name: 'WebEditor',
          description: 'Modulo de WebEditor',
        },
        {
          name: 'System',
          description: 'Modulo de System',
        },
        {
          name: 'Empleados',
          description: 'Modulo de Empleados',
        },
      ];
      modules.forEach((element) => {
        this.moduleRepo.create(element);
      });
      return {message: 'done'};
    } else {
      return {message: 'Ya estan los modulos'};
    }
  }
  /**
   * This method will be invoked when the application stops
   */
  async stop(): Promise<void> {
    // Add your logic for stop
  }
}

import fs from 'fs';

// TEMPORAL URLS
export const HOST_ADMIN = "https://admin.santandermiacademia.guaodev.com";
export const HOST = "https://santandermiacademia.guaodev.com"

// Html templates for mailing
export const PASSWORD_RECOVERY_TEMPLATE = fs.readFileSync('./src/templates/password-recovery.html', 'utf8');
export const PASSWORD_SUCCESS_RECOVERY_TEMPLATE = fs.readFileSync('./src/templates/success-password-recovery.html', 'utf8');

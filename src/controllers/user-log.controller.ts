import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  User,
  Log,
} from '../models';
import {UserRepository} from '../repositories';

export class UserLogController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) { }

  @get('/users/{id}/logs', {
    responses: {
      '200': {
        description: 'Array of User has many Log',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Log)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Log>,
  ): Promise<Log[]> {
    return this.userRepository.logs(id).find(filter);
  }

  @post('/users/{id}/logs', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(Log)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Log, {
            title: 'NewLogInUser',
            exclude: ['id'],
            optional: ['userId']
          }),
        },
      },
    }) log: Omit<Log, 'id'>,
  ): Promise<Log> {
    return this.userRepository.logs(id).create(log);
  }

  @patch('/users/{id}/logs', {
    responses: {
      '200': {
        description: 'User.Log PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Log, {partial: true}),
        },
      },
    })
    log: Partial<Log>,
    @param.query.object('where', getWhereSchemaFor(Log)) where?: Where<Log>,
  ): Promise<Count> {
    return this.userRepository.logs(id).patch(log, where);
  }

  @del('/users/{id}/logs', {
    responses: {
      '200': {
        description: 'User.Log DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Log)) where?: Where<Log>,
  ): Promise<Count> {
    return this.userRepository.logs(id).delete(where);
  }
}

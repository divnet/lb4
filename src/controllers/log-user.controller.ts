import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Log,
  User,
} from '../models';
import {LogRepository} from '../repositories';

export class LogUserController {
  constructor(
    @repository(LogRepository)
    public logRepository: LogRepository,
  ) { }

  @get('/logs/{id}/user', {
    responses: {
      '200': {
        description: 'User belonging to Log',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof Log.prototype.id,
  ): Promise<User> {
    return this.logRepository.user(id);
  }
}

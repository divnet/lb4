import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, getWhereSchemaFor, param, patch, post, requestBody } from '@loopback/rest'

import { Module, RoleModule } from '../models'
import { ModuleRepository } from '../repositories'

@authenticate('jwt')
export class ModuleRoleModuleController {
  constructor(
    @repository(ModuleRepository) protected moduleRepository: ModuleRepository,
  ) {}

  @get('/modules/{id}/role-modules', {
    responses: {
      '200': {
        description: 'Array of Module has many RoleModule',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(RoleModule)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<RoleModule>,
  ): Promise<RoleModule[]> {
    return this.moduleRepository.roleModules(id).find(filter);
  }

  @post('/modules/{id}/role-modules', {
    responses: {
      '200': {
        description: 'Module model instance',
        content: {'application/json': {schema: getModelSchemaRef(RoleModule)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Module.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleModule, {
            title: 'NewRoleModuleInModule',
            exclude: ['id'],
            optional: ['moduleId'],
          }),
        },
      },
    })
    roleModule: Omit<RoleModule, 'id'>,
  ): Promise<RoleModule> {
    return this.moduleRepository.roleModules(id).create(roleModule);
  }

  @patch('/modules/{id}/role-modules', {
    responses: {
      '200': {
        description: 'Module.RoleModule PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleModule, {partial: true}),
        },
      },
    })
    roleModule: Partial<RoleModule>,
    @param.query.object('where', getWhereSchemaFor(RoleModule))
    where?: Where<RoleModule>,
  ): Promise<Count> {
    return this.moduleRepository.roleModules(id).patch(roleModule, where);
  }

  @del('/modules/{id}/role-modules', {
    responses: {
      '200': {
        description: 'Module.RoleModule DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(RoleModule))
    where?: Where<RoleModule>,
  ): Promise<Count> {
    return this.moduleRepository.roleModules(id).delete(where);
  }
}

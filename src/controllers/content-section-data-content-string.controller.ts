import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, getWhereSchemaFor, param, patch, post, requestBody } from '@loopback/rest'

import { ContentSection, DataContentString } from '../models'
import { ContentSectionRepository } from '../repositories'

@authenticate('jwt')
export class ContentSectionDataContentStringController {
  constructor(
    @repository(ContentSectionRepository)
    protected contentSectionRepository: ContentSectionRepository,
  ) {}

  @get('/content-sections/{id}/data-content-strings', {
    responses: {
      '200': {
        description: 'Array of ContentSection has many DataContentString',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataContentString),
            },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<DataContentString>,
  ): Promise<DataContentString[]> {
    return this.contentSectionRepository.dataContentStrings(id).find(filter);
  }

  @post('/content-sections/{id}/data-content-strings', {
    responses: {
      '200': {
        description: 'ContentSection model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(DataContentString)},
        },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof ContentSection.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentString, {
            title: 'NewDataContentStringInContentSection',
            exclude: ['id'],
            optional: ['contentSectionId'],
          }),
        },
      },
    })
    dataContentString: Omit<DataContentString, 'id'>,
  ): Promise<DataContentString> {
    return this.contentSectionRepository
      .dataContentStrings(id)
      .create(dataContentString);
  }

  @patch('/content-sections/{id}/data-content-strings', {
    responses: {
      '200': {
        description: 'ContentSection.DataContentString PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentString, {partial: true}),
        },
      },
    })
    dataContentString: Partial<DataContentString>,
    @param.query.object('where', getWhereSchemaFor(DataContentString))
    where?: Where<DataContentString>,
  ): Promise<Count> {
    return this.contentSectionRepository
      .dataContentStrings(id)
      .patch(dataContentString, where);
  }

  @del('/content-sections/{id}/data-content-strings', {
    responses: {
      '200': {
        description: 'ContentSection.DataContentString DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(DataContentString))
    where?: Where<DataContentString>,
  ): Promise<Count> {
    return this.contentSectionRepository.dataContentStrings(id).delete(where);
  }
}

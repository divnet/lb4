import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { AppUserData, User } from '../models'
import { AppUserDataRepository } from '../repositories'

@authenticate('jwt')
export class AppUserDataUserController {
  constructor(
    @repository(AppUserDataRepository)
    public appUserDataRepository: AppUserDataRepository,
  ) {}

  @get('/app-user-data/{id}/user', {
    responses: {
      '200': {
        description: 'User belonging to AppUserData',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof AppUserData.prototype.id,
  ): Promise<User> {
    return this.appUserDataRepository.user(id);
  }
}

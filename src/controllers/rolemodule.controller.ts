import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import {
  del,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest'

import { RoleModule } from '../models'
import { RoleModuleRepository } from '../repositories'

@authenticate('jwt')
export class RoleModuleController {
  constructor(
    @repository(RoleModuleRepository)
    public roleModuleRepository: RoleModuleRepository,
  ) {}

  @post('/role-modules', {
    responses: {
      '200': {
        description: 'RoleModule model instance',
        content: {'application/json': {schema: getModelSchemaRef(RoleModule)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleModule, {
            title: 'NewRoleModule',
            exclude: ['id'],
          }),
        },
      },
    })
    roleModule: Omit<RoleModule, 'id'>,
  ): Promise<RoleModule> {
    return this.roleModuleRepository.create(roleModule);
  }

  @get('/role-modules/count', {
    responses: {
      '200': {
        description: 'RoleModule model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(RoleModule))
    where?: Where<RoleModule>,
  ): Promise<Count> {
    return this.roleModuleRepository.count(where);
  }

  @get('/role-modules', {
    responses: {
      '200': {
        description: 'Array of RoleModule model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(RoleModule, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(RoleModule))
    filter?: Filter<RoleModule>,
  ): Promise<RoleModule[]> {
    return this.roleModuleRepository.find(filter);
  }

  @patch('/role-modules', {
    responses: {
      '200': {
        description: 'RoleModule PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleModule, {partial: true}),
        },
      },
    })
    roleModule: RoleModule,
    @param.query.object('where', getWhereSchemaFor(RoleModule))
    where?: Where<RoleModule>,
  ): Promise<Count> {
    return this.roleModuleRepository.updateAll(roleModule, where);
  }

  @get('/role-modules/{id}', {
    responses: {
      '200': {
        description: 'RoleModule model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(RoleModule, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(RoleModule))
    filter?: Filter<RoleModule>,
  ): Promise<RoleModule> {
    return this.roleModuleRepository.findById(id, filter);
  }

  @patch('/role-modules/{id}', {
    responses: {
      '204': {
        description: 'RoleModule PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RoleModule, {partial: true}),
        },
      },
    })
    roleModule: RoleModule,
  ): Promise<void> {
    await this.roleModuleRepository.updateById(id, roleModule);
  }

  @put('/role-modules/{id}', {
    responses: {
      '204': {
        description: 'RoleModule PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() roleModule: RoleModule,
  ): Promise<void> {
    await this.roleModuleRepository.replaceById(id, roleModule);
  }

  @del('/role-modules/{id}', {
    responses: {
      '204': {
        description: 'RoleModule DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.roleModuleRepository.deleteById(id);
  }
}

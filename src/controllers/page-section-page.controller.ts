import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, getWhereSchemaFor, param, patch, post, requestBody } from '@loopback/rest'

import { Page, SectionPage } from '../models'
import { PageRepository } from '../repositories'

@authenticate('jwt')
export class PageSectionPageController {
  constructor(
    @repository(PageRepository) protected pageRepository: PageRepository,
  ) {}

  @get('/pages/{id}/section-pages', {
    responses: {
      '200': {
        description: 'Array of Page has many SectionPage',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(SectionPage)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<SectionPage>,
  ): Promise<SectionPage[]> {
    return this.pageRepository.sectionPages(id).find(filter);
  }

  @post('/pages/{id}/section-pages', {
    responses: {
      '200': {
        description: 'Page model instance',
        content: {'application/json': {schema: getModelSchemaRef(SectionPage)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Page.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SectionPage, {
            title: 'NewSectionPageInPage',
            exclude: ['id'],
            optional: ['pageId'],
          }),
        },
      },
    })
    sectionPage: Omit<SectionPage, 'id'>,
  ): Promise<SectionPage> {
    return this.pageRepository.sectionPages(id).create(sectionPage);
  }

  @patch('/pages/{id}/section-pages', {
    responses: {
      '200': {
        description: 'Page.SectionPage PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SectionPage, {partial: true}),
        },
      },
    })
    sectionPage: Partial<SectionPage>,
    @param.query.object('where', getWhereSchemaFor(SectionPage))
    where?: Where<SectionPage>,
  ): Promise<Count> {
    return this.pageRepository.sectionPages(id).patch(sectionPage, where);
  }

  @del('/pages/{id}/section-pages', {
    responses: {
      '200': {
        description: 'Page.SectionPage DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(SectionPage))
    where?: Where<SectionPage>,
  ): Promise<Count> {
    return this.pageRepository.sectionPages(id).delete(where);
  }
}

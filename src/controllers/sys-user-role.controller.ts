import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { SysUserRole } from '../models'
import { SysUserRoleRepository } from '../repositories'

@authenticate('jwt')
export class SysUserRoleController {
  constructor(
    @repository(SysUserRoleRepository)
    public sysUserRoleRepository: SysUserRoleRepository,
  ) {}

  @post('/sys-user-roles', {
    responses: {
      '200': {
        description: 'SysUserRole model instance',
        content: {'application/json': {schema: getModelSchemaRef(SysUserRole)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserRole, {
            title: 'NewSysUserRole',
            exclude: ['id'],
          }),
        },
      },
    })
    sysUserRole: Omit<SysUserRole, 'id'>,
  ): Promise<SysUserRole> {
    return this.sysUserRoleRepository.create(sysUserRole);
  }

  @get('/sys-user-roles/count', {
    responses: {
      '200': {
        description: 'SysUserRole model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(SysUserRole) where?: Where<SysUserRole>,
  ): Promise<Count> {
    return this.sysUserRoleRepository.count(where);
  }

  @get('/sys-user-roles', {
    responses: {
      '200': {
        description: 'Array of SysUserRole model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SysUserRole, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(SysUserRole) filter?: Filter<SysUserRole>,
  ): Promise<SysUserRole[]> {
    return this.sysUserRoleRepository.find(filter);
  }

  @patch('/sys-user-roles', {
    responses: {
      '200': {
        description: 'SysUserRole PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserRole, {partial: true}),
        },
      },
    })
    sysUserRole: SysUserRole,
    @param.where(SysUserRole) where?: Where<SysUserRole>,
  ): Promise<Count> {
    return this.sysUserRoleRepository.updateAll(sysUserRole, where);
  }

  @get('/sys-user-roles/{id}', {
    responses: {
      '200': {
        description: 'SysUserRole model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SysUserRole, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(SysUserRole, {exclude: 'where'})
    filter?: FilterExcludingWhere<SysUserRole>,
  ): Promise<SysUserRole> {
    return this.sysUserRoleRepository.findById(id, filter);
  }

  @patch('/sys-user-roles/{id}', {
    responses: {
      '204': {
        description: 'SysUserRole PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserRole, {partial: true}),
        },
      },
    })
    sysUserRole: SysUserRole,
  ): Promise<void> {
    await this.sysUserRoleRepository.updateById(id, sysUserRole);
  }

  @put('/sys-user-roles/{id}', {
    responses: {
      '204': {
        description: 'SysUserRole PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() sysUserRole: SysUserRole,
  ): Promise<void> {
    await this.sysUserRoleRepository.replaceById(id, sysUserRole);
  }

  @del('/sys-user-roles/{id}', {
    responses: {
      '204': {
        description: 'SysUserRole DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.sysUserRoleRepository.deleteById(id);
  }
}

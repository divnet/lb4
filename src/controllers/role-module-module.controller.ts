import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { Module, RoleModule } from '../models'
import { RoleModuleRepository } from '../repositories'

@authenticate('jwt')
export class RoleModuleModuleController {
  constructor(
    @repository(RoleModuleRepository)
    public roleModuleRepository: RoleModuleRepository,
  ) {}

  @get('/role-modules/{id}/module', {
    responses: {
      '200': {
        description: 'Module belonging to RoleModule',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Module)},
          },
        },
      },
    },
  })
  async getModule(
    @param.path.string('id') id: typeof RoleModule.prototype.id,
  ): Promise<Module> {
    return this.roleModuleRepository.module(id);
  }
}

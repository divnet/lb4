import { repository } from '@loopback/repository'
import { get, param } from '@loopback/rest'

import {
  AppUserDataRepository,
  ContentSectionRepository,
  DataContentFileRepository,
  DataContentStringRepository,
  OrganizationRepository,
  PageRepository,
  SectionPageRepository,
  UserRepository,
} from '../repositories'

// Uncomment these imports to begin using these cool features!
export class ToolController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(AppUserDataRepository)
    public appUserRepository: AppUserDataRepository,
    @repository(OrganizationRepository)
    public orgRepository: OrganizationRepository,
    @repository(PageRepository)
    public pageRepository: PageRepository,
    @repository(SectionPageRepository)
    public sectionRepository: SectionPageRepository,
    @repository(ContentSectionRepository)
    public contentRepository: ContentSectionRepository,
    @repository(DataContentFileRepository)
    public dataFileRepository: DataContentFileRepository,
    @repository(DataContentStringRepository)
    public dataStringRepository: DataContentStringRepository,
  ) {}

  @get('/tools/getPages/{name}', {
    responses: {
      '200': {
        description: 'Return page by Organization ID',
        content: {
          'application/json': {
            schema: {
              type: 'array',
            },
          },
        },
      },
    },
  })
  async find(@param.path.string('name') name: string) {
    const findByName = {where: {name: name}, include: [{relation: 'pages'}]};
    const orgData = await this.orgRepository.find(findByName);
    console.log(orgData);
    if (orgData) {
      const idF = orgData[0].id;
      console.log(idF);
      const pageResult = await this.orgRepository.pages(idF).find();
      console.log(pageResult);
      const finalResult = {
        orgData,
        pageResult,
      };
      return finalResult;
    } else {
      const message = {message: '404 Not found'};
      return message;
    }
  }

  @get('/tools/getFullContentOf/{id}', {
    responses: {
      '200': {
        description: 'Returns all data of the page by page id',
        content: {
          'application/json': {
            schema: {},
          },
        },
      },
    },
  })
  async findById2(@param.path.string('id') id: string) {
    const pageData = await this.pageRepository.sectionPages(id).find({
      include: [
        {
          relation: 'contentSections',
          scope: {
            fields: {},
            include: [
              {relation: 'dataContentStrings'},
              {relation: 'dataContentFiles'},
            ],
          },
        },
      ],
    });
    console.log(pageData);
    return pageData;
  }
}

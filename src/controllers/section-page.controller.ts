import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {SectionPage} from '../models';
import {SectionPageRepository} from '../repositories';

export class SectionPageController {
  constructor(
    @repository(SectionPageRepository)
    public sectionPageRepository : SectionPageRepository,
  ) {}

  @post('/section-pages', {
    responses: {
      '200': {
        description: 'SectionPage model instance',
        content: {'application/json': {schema: getModelSchemaRef(SectionPage)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SectionPage, {
            title: 'NewSectionPage',
            exclude: ['id'],
          }),
        },
      },
    })
    sectionPage: Omit<SectionPage, 'id'>,
  ): Promise<SectionPage> {
    return this.sectionPageRepository.create(sectionPage);
  }

  @get('/section-pages/count', {
    responses: {
      '200': {
        description: 'SectionPage model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(SectionPage) where?: Where<SectionPage>,
  ): Promise<Count> {
    return this.sectionPageRepository.count(where);
  }

  @get('/section-pages', {
    responses: {
      '200': {
        description: 'Array of SectionPage model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SectionPage, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(SectionPage) filter?: Filter<SectionPage>,
  ): Promise<SectionPage[]> {
    return this.sectionPageRepository.find(filter);
  }

  @patch('/section-pages', {
    responses: {
      '200': {
        description: 'SectionPage PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SectionPage, {partial: true}),
        },
      },
    })
    sectionPage: SectionPage,
    @param.where(SectionPage) where?: Where<SectionPage>,
  ): Promise<Count> {
    return this.sectionPageRepository.updateAll(sectionPage, where);
  }

  @get('/section-pages/{id}', {
    responses: {
      '200': {
        description: 'SectionPage model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SectionPage, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(SectionPage, {exclude: 'where'}) filter?: FilterExcludingWhere<SectionPage>
  ): Promise<SectionPage> {
    return this.sectionPageRepository.findById(id, filter);
  }

  @patch('/section-pages/{id}', {
    responses: {
      '204': {
        description: 'SectionPage PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SectionPage, {partial: true}),
        },
      },
    })
    sectionPage: SectionPage,
  ): Promise<void> {
    await this.sectionPageRepository.updateById(id, sectionPage);
  }

  @put('/section-pages/{id}', {
    responses: {
      '204': {
        description: 'SectionPage PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() sectionPage: SectionPage,
  ): Promise<void> {
    await this.sectionPageRepository.replaceById(id, sectionPage);
  }

  @del('/section-pages/{id}', {
    responses: {
      '204': {
        description: 'SectionPage DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.sectionPageRepository.deleteById(id);
  }
}

import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { BlogEntry } from '../models'
import { BlogEntryRepositoryRepository } from '../repositories'

@authenticate('jwt')
export class BlogEntryController {
  constructor(
    @repository(BlogEntryRepositoryRepository)
    public blogEntryRepositoryRepository: BlogEntryRepositoryRepository,
  ) {}

  @post('/blog-entries', {
    responses: {
      '200': {
        description: 'BlogEntry model instance',
        content: {'application/json': {schema: getModelSchemaRef(BlogEntry)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BlogEntry, {
            title: 'NewBlogEntry',
            exclude: ['id'],
          }),
        },
      },
    })
    blogEntry: Omit<BlogEntry, 'id'>,
  ): Promise<BlogEntry> {
    return this.blogEntryRepositoryRepository.create(blogEntry);
  }

  @get('/blog-entries/count', {
    responses: {
      '200': {
        description: 'BlogEntry model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(BlogEntry) where?: Where<BlogEntry>,
  ): Promise<Count> {
    return this.blogEntryRepositoryRepository.count(where);
  }

  @get('/blog-entries', {
    responses: {
      '200': {
        description: 'Array of BlogEntry model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(BlogEntry, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  @authenticate.skip()
  async find(
    @param.filter(BlogEntry) filter?: Filter<BlogEntry>,
  ): Promise<BlogEntry[]> {
    return this.blogEntryRepositoryRepository.find(filter);
  }

  @patch('/blog-entries', {
    responses: {
      '200': {
        description: 'BlogEntry PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BlogEntry, {partial: true}),
        },
      },
    })
    blogEntry: BlogEntry,
    @param.where(BlogEntry) where?: Where<BlogEntry>,
  ): Promise<Count> {
    return this.blogEntryRepositoryRepository.updateAll(blogEntry, where);
  }

  @get('/blog-entries/{id}', {
    responses: {
      '200': {
        description: 'BlogEntry model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BlogEntry, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(BlogEntry, {exclude: 'where'})
    filter?: FilterExcludingWhere<BlogEntry>,
  ): Promise<BlogEntry> {
    return this.blogEntryRepositoryRepository.findById(id, filter);
  }

  @patch('/blog-entries/{id}', {
    responses: {
      '204': {
        description: 'BlogEntry PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BlogEntry, {partial: true}),
        },
      },
    })
    blogEntry: BlogEntry,
  ): Promise<void> {
    await this.blogEntryRepositoryRepository.updateById(id, blogEntry);
  }

  @put('/blog-entries/{id}', {
    responses: {
      '204': {
        description: 'BlogEntry PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() blogEntry: BlogEntry,
  ): Promise<void> {
    await this.blogEntryRepositoryRepository.replaceById(id, blogEntry);
  }

  @del('/blog-entries/{id}', {
    responses: {
      '204': {
        description: 'BlogEntry DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.blogEntryRepositoryRepository.deleteById(id);
  }
}

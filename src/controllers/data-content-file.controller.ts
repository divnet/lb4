import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { DataContentFile } from '../models'
import { DataContentFileRepository } from '../repositories'

@authenticate('jwt')
export class DataContentFileController {
  constructor(
    @repository(DataContentFileRepository)
    public dataContentImageRepository: DataContentFileRepository,
  ) {}

  @post('/data-content-files', {
    responses: {
      '200': {
        description: 'DataContentFile model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(DataContentFile)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentFile, {
            title: 'NewDataContentFile',
            exclude: ['id'],
          }),
        },
      },
    })
    dataContentFile: Omit<DataContentFile, 'id'>,
  ): Promise<DataContentFile> {
    return this.dataContentImageRepository.create(dataContentFile);
  }

  @get('/data-content-files/count', {
    responses: {
      '200': {
        description: 'DataContentFile model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataContentFile) where?: Where<DataContentFile>,
  ): Promise<Count> {
    return this.dataContentImageRepository.count(where);
  }

  @get('/data-content-files', {
    responses: {
      '200': {
        description: 'Array of DataContentFile model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataContentFile, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataContentFile) filter?: Filter<DataContentFile>,
  ): Promise<DataContentFile[]> {
    return this.dataContentImageRepository.find(filter);
  }

  @patch('/data-content-files', {
    responses: {
      '200': {
        description: 'DataContentFile PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentFile, {partial: true}),
        },
      },
    })
    dataContentFile: DataContentFile,
    @param.where(DataContentFile) where?: Where<DataContentFile>,
  ): Promise<Count> {
    return this.dataContentImageRepository.updateAll(dataContentFile, where);
  }

  @get('/data-content-files/{id}', {
    responses: {
      '200': {
        description: 'DataContentFile model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataContentFile, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(DataContentFile, {exclude: 'where'})
    filter?: FilterExcludingWhere<DataContentFile>,
  ): Promise<DataContentFile> {
    return this.dataContentImageRepository.findById(id, filter);
  }

  @patch('/data-content-files/{id}', {
    responses: {
      '204': {
        description: 'DataContentFile PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentFile, {partial: true}),
        },
      },
    })
    dataContentFile: DataContentFile,
  ): Promise<void> {
    await this.dataContentImageRepository.updateById(id, dataContentFile);
  }

  @put('/data-content-files/{id}', {
    responses: {
      '204': {
        description: 'DataContentFile PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() dataContentFile: DataContentFile,
  ): Promise<void> {
    await this.dataContentImageRepository.replaceById(id, dataContentFile);
  }

  @del('/data-content-files/{id}', {
    responses: {
      '204': {
        description: 'DataContentFile DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.dataContentImageRepository.deleteById(id);
  }
}

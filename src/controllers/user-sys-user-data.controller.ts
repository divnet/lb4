import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { SysUserData, User } from '../models'
import { UserRepository } from '../repositories'

@authenticate('jwt')
export class UserSysUserDataController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  @get('/users/{id}/sys-user-data', {
    responses: {
      '200': {
        description: 'SysUserData belonging to User',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(SysUserData)},
          },
        },
      },
    },
  })
  async getSysUserData(
    @param.path.string('id') id: typeof User.prototype.id,
  ): Promise<SysUserData> {
    return this.userRepository.sysUserData(id);
  }
}

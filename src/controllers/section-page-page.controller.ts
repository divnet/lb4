import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { Page, SectionPage } from '../models'
import { SectionPageRepository } from '../repositories'

@authenticate('jwt')
export class SectionPagePageController {
  constructor(
    @repository(SectionPageRepository)
    public sectionPageRepository: SectionPageRepository,
  ) {}

  @get('/section-pages/{id}/page', {
    responses: {
      '200': {
        description: 'Page belonging to SectionPage',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Page)},
          },
        },
      },
    },
  })
  async getPage(
    @param.path.string('id') id: typeof SectionPage.prototype.id,
  ): Promise<Page> {
    return this.sectionPageRepository.page(id);
  }
}

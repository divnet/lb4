import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { SysUserData } from '../models'
import { SysUserDataRepository } from '../repositories'

@authenticate('jwt')
export class SysUserDataController {
  constructor(
    @repository(SysUserDataRepository)
    public sysUserDataRepository: SysUserDataRepository,
  ) {}

  @post('/sys-user-data', {
    responses: {
      '200': {
        description: 'SysUserData model instance',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SysUserData, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserData, {
            title: 'NewSysUserData',
            exclude: ['id'],
          }),
        },
      },
    })
    sysUserData: Omit<SysUserData, 'id'>,
  ): Promise<SysUserData> {
    return this.sysUserDataRepository.create(sysUserData);
  }

  @get('/sys-user-data/count', {
    responses: {
      '200': {
        description: 'SysUserData model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(SysUserData) where?: Where<SysUserData>,
  ): Promise<Count> {
    return this.sysUserDataRepository.count(where);
  }

  @get('/sys-user-data', {
    responses: {
      '200': {
        description: 'Array of SysUserData model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SysUserData, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(SysUserData) filter?: Filter<SysUserData>,
  ): Promise<SysUserData[]> {
    return this.sysUserDataRepository.find(filter);
  }

  @patch('/sys-user-data', {
    responses: {
      '200': {
        description: 'SysUserData PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserData, {partial: true}),
        },
      },
    })
    sysUserData: SysUserData,
    @param.where(SysUserData) where?: Where<SysUserData>,
  ): Promise<Count> {
    return this.sysUserDataRepository.updateAll(sysUserData, where);
  }

  @get('/sys-user-data/{id}', {
    responses: {
      '200': {
        description: 'SysUserData model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SysUserData, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(SysUserData, {exclude: 'where'})
    filter?: FilterExcludingWhere<SysUserData>,
  ): Promise<SysUserData> {
    return this.sysUserDataRepository.findById(id, filter);
  }

  @patch('/sys-user-data/{id}', {
    responses: {
      '204': {
        description: 'SysUserData PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserData, {partial: true}),
        },
      },
    })
    sysUserData: SysUserData,
  ): Promise<void> {
    await this.sysUserDataRepository.updateById(id, sysUserData);
  }

  @put('/sys-user-data/{id}', {
    responses: {
      '204': {
        description: 'SysUserData PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() sysUserData: SysUserData,
  ): Promise<void> {
    await this.sysUserDataRepository.replaceById(id, sysUserData);
  }

  @del('/sys-user-data/{id}', {
    responses: {
      '204': {
        description: 'SysUserData DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.sysUserDataRepository.deleteById(id);
  }
}

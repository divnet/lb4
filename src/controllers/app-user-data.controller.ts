import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { AppUserData } from '../models'
import { AppUserDataRepository } from '../repositories'

@authenticate('jwt')
export class AppUserDataController {
  constructor(
    @repository(AppUserDataRepository)
    public appUserDataRepository: AppUserDataRepository,
  ) {}

  @post('/app-user-data', {
    responses: {
      '200': {
        description: 'AppUserData model instance',
        content: {'application/json': {schema: getModelSchemaRef(AppUserData)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AppUserData, {
            title: 'NewAppUserData',
            exclude: ['id'],
          }),
        },
      },
    })
    appUserData: Omit<AppUserData, 'id'>,
  ): Promise<AppUserData> {
    return this.appUserDataRepository.create(appUserData);
  }

  @get('/app-user-data/count', {
    responses: {
      '200': {
        description: 'AppUserData model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(AppUserData) where?: Where<AppUserData>,
  ): Promise<Count> {
    return this.appUserDataRepository.count(where);
  }

  @get('/app-user-data', {
    responses: {
      '200': {
        description: 'Array of AppUserData model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AppUserData, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  //
  async find(
    @param.filter(AppUserData) filter?: Filter<AppUserData>,
  ): Promise<Array<Object>> {
    return this.appUserDataRepository.find(filter);
  }

  @patch('/app-user-data', {
    responses: {
      '200': {
        description: 'AppUserData PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AppUserData, {partial: true}),
        },
      },
    })
    appUserData: AppUserData,
    @param.where(AppUserData) where?: Where<AppUserData>,
  ): Promise<Count> {
    return this.appUserDataRepository.updateAll(appUserData, where);
  }

  @get('/app-user-data/{id}', {
    responses: {
      '200': {
        description: 'AppUserData model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AppUserData, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(AppUserData, {exclude: 'where'})
    filter?: FilterExcludingWhere<AppUserData>,
  ): Promise<AppUserData> {
    return this.appUserDataRepository.findById(id, filter);
  }

  @patch('/app-user-data/{id}', {
    responses: {
      '204': {
        description: 'AppUserData PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AppUserData, {partial: true}),
        },
      },
    })
    appUserData: AppUserData,
  ): Promise<void> {
    await this.appUserDataRepository.updateById(id, appUserData);
  }

  @put('/app-user-data/{id}', {
    responses: {
      '204': {
        description: 'AppUserData PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() appUserData: AppUserData,
  ): Promise<void> {
    await this.appUserDataRepository.replaceById(id, appUserData);
  }

  @del('/app-user-data/{id}', {
    responses: {
      '204': {
        description: 'AppUserData DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.appUserDataRepository.deleteById(id);
  }
}

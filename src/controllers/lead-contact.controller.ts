import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import {
  del,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest'

import { LeadContent } from '../models'
import { LeadContentRepository } from '../repositories'

@authenticate('jwt')
export class LeadContactController {
  constructor(
    @repository(LeadContentRepository)
    public leadContentRepository: LeadContentRepository,
  ) {}

  @post('/lead-contents', {
    responses: {
      '200': {
        description: 'LeadContent model instance',
        content: {'application/json': {schema: getModelSchemaRef(LeadContent)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LeadContent, {
            title: 'NewLeadContent',
            exclude: ['id'],
          }),
        },
      },
    })
    leadContent: Omit<LeadContent, 'id'>,
  ): Promise<LeadContent> {
    return this.leadContentRepository.create(leadContent);
  }

  @get('/lead-contents/count', {
    responses: {
      '200': {
        description: 'LeadContent model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(LeadContent))
    where?: Where<LeadContent>,
  ): Promise<Count> {
    return this.leadContentRepository.count(where);
  }

  @get('/lead-contents', {
    responses: {
      '200': {
        description: 'Array of LeadContent model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(LeadContent, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(LeadContent))
    filter?: Filter<LeadContent>,
  ): Promise<LeadContent[]> {
    return this.leadContentRepository.find(filter);
  }

  @patch('/lead-contents', {
    responses: {
      '200': {
        description: 'LeadContent PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LeadContent, {partial: true}),
        },
      },
    })
    leadContent: LeadContent,
    @param.query.object('where', getWhereSchemaFor(LeadContent))
    where?: Where<LeadContent>,
  ): Promise<Count> {
    return this.leadContentRepository.updateAll(leadContent, where);
  }

  @get('/lead-contents/{id}', {
    responses: {
      '200': {
        description: 'LeadContent model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(LeadContent, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(LeadContent))
    filter?: Filter<LeadContent>,
  ): Promise<LeadContent> {
    return this.leadContentRepository.findById(id, filter);
  }

  @patch('/lead-contents/{id}', {
    responses: {
      '204': {
        description: 'LeadContent PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LeadContent, {partial: true}),
        },
      },
    })
    leadContent: LeadContent,
  ): Promise<void> {
    await this.leadContentRepository.updateById(id, leadContent);
  }

  @put('/lead-contents/{id}', {
    responses: {
      '204': {
        description: 'LeadContent PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() leadContent: LeadContent,
  ): Promise<void> {
    await this.leadContentRepository.replaceById(id, leadContent);
  }

  @del('/lead-contents/{id}', {
    responses: {
      '204': {
        description: 'LeadContent DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.leadContentRepository.deleteById(id);
  }
}

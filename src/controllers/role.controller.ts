import { authenticate } from '@loopback/authentication'
import { Filter, repository } from '@loopback/repository'
import { get, getFilterSchemaFor, getModelSchemaRef, param, post, requestBody } from '@loopback/rest'

import { Role } from '../models'
import { RoleRepository } from '../repositories'

@authenticate('jwt')
export class RoleController {
  constructor(
    @repository(RoleRepository) private roleRepository: RoleRepository,
  ) {}

  @get('/roles', {
    responses: {
      '200': {
        description: 'Array of Roles model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Role, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Role))
    filter?: Filter<Role>,
  ): Promise<Role[]> {
    return this.roleRepository.find(filter);
  }

  @post('/roels')
  async createRole(@requestBody() role: Role): Promise<Role> {
    return this.roleRepository.create(role);
  }
}

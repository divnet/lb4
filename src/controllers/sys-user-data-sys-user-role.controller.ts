import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { SysUserData, SysUserRole } from '../models'
import { SysUserDataRepository } from '../repositories'

@authenticate('jwt')
export class SysUserDataSysUserRoleController {
  constructor(
    @repository(SysUserDataRepository)
    public sysUserDataRepository: SysUserDataRepository,
  ) {}

  @get('/sys-user-data/{id}/sys-user-role', {
    responses: {
      '200': {
        description: 'SysUserRole belonging to SysUserData',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(SysUserRole)},
          },
        },
      },
    },
  })
  async getSysUserRole(
    @param.path.string('id') id: typeof SysUserData.prototype.id,
  ): Promise<SysUserRole> {
    return this.sysUserDataRepository.sysUserRole(id);
  }
}

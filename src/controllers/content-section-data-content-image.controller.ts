import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, getWhereSchemaFor, param, patch, post, requestBody } from '@loopback/rest'

import { ContentSection, DataContentFile } from '../models'
import { ContentSectionRepository } from '../repositories'

@authenticate('jwt')
export class ContentSectionDataContentFileController {
  constructor(
    @repository(ContentSectionRepository)
    protected contentSectionRepository: ContentSectionRepository,
  ) {}

  @get('/content-sections/{id}/data-content-files', {
    responses: {
      '200': {
        description: 'Array of ContentSection has many DataContentFile',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(DataContentFile)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<DataContentFile>,
  ): Promise<DataContentFile[]> {
    return this.contentSectionRepository.dataContentFiles(id).find(filter);
  }

  @post('/content-sections/{id}/data-content-files', {
    responses: {
      '200': {
        description: 'ContentSection model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(DataContentFile)},
        },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof ContentSection.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentFile, {
            title: 'NewDataContentFileInContentSection',
            exclude: ['id'],
            optional: ['contentSectionId'],
          }),
        },
      },
    })
    dataContentFile: Omit<DataContentFile, 'id'>,
  ): Promise<DataContentFile> {
    return this.contentSectionRepository
      .dataContentFiles(id)
      .create(dataContentFile);
  }

  @patch('/content-sections/{id}/data-content-files', {
    responses: {
      '200': {
        description: 'ContentSection.DataContentImage PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentFile, {partial: true}),
        },
      },
    })
    dataContentFile: Partial<DataContentFile>,
    @param.query.object('where', getWhereSchemaFor(DataContentFile))
    where?: Where<DataContentFile>,
  ): Promise<Count> {
    return this.contentSectionRepository
      .dataContentFiles(id)
      .patch(dataContentFile, where);
  }

  @del('/content-sections/{id}/data-content-file', {
    responses: {
      '200': {
        description: 'ContentSection.DataContentFile DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(DataContentFile))
    where?: Where<DataContentFile>,
  ): Promise<Count> {
    return this.contentSectionRepository.dataContentFiles(id).delete(where);
  }
}

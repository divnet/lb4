import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, getWhereSchemaFor, param, patch, post, requestBody } from '@loopback/rest'

import { SysUserData, SysUserRole } from '../models'
import { SysUserRoleRepository } from '../repositories'

@authenticate('jwt')
export class SysUserRoleSysUserDataController {
  constructor(
    @repository(SysUserRoleRepository)
    protected sysUserRoleRepository: SysUserRoleRepository,
  ) {}

  @get('/sys-user-roles/{id}/sys-user-data', {
    responses: {
      '200': {
        description: 'SysUserRole has one SysUserData',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SysUserData),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<SysUserData>,
  ): Promise<SysUserData> {
    return this.sysUserRoleRepository.sysUserData(id).get(filter);
  }

  @post('/sys-user-roles/{id}/sys-user-data', {
    responses: {
      '200': {
        description: 'SysUserRole model instance',
        content: {'application/json': {schema: getModelSchemaRef(SysUserData)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof SysUserRole.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserData, {
            title: 'NewSysUserDataInSysUserRole',
            exclude: ['id'],
            optional: ['sysUserRoleId'],
          }),
        },
      },
    })
    sysUserData: Omit<SysUserData, 'id'>,
  ): Promise<SysUserData> {
    return this.sysUserRoleRepository.sysUserData(id).create(sysUserData);
  }

  @patch('/sys-user-roles/{id}/sys-user-data', {
    responses: {
      '200': {
        description: 'SysUserRole.SysUserData PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserData, {partial: true}),
        },
      },
    })
    sysUserData: Partial<SysUserData>,
    @param.query.object('where', getWhereSchemaFor(SysUserData))
    where?: Where<SysUserData>,
  ): Promise<Count> {
    return this.sysUserRoleRepository.sysUserData(id).patch(sysUserData, where);
  }

  @del('/sys-user-roles/{id}/sys-user-data', {
    responses: {
      '200': {
        description: 'SysUserRole.SysUserData DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(SysUserData))
    where?: Where<SysUserData>,
  ): Promise<Count> {
    return this.sysUserRoleRepository.sysUserData(id).delete(where);
  }
}

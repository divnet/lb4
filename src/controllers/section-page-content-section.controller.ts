import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, getWhereSchemaFor, param, patch, post, requestBody } from '@loopback/rest'

import { ContentSection, SectionPage } from '../models'
import { SectionPageRepository } from '../repositories'

@authenticate('jwt')
export class SectionPageContentSectionController {
  constructor(
    @repository(SectionPageRepository)
    protected sectionPageRepository: SectionPageRepository,
  ) {}

  @get('/section-pages/{id}/content-sections', {
    responses: {
      '200': {
        description: 'Array of SectionPage has many ContentSection',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(ContentSection)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<ContentSection>,
  ): Promise<ContentSection[]> {
    return this.sectionPageRepository.contentSections(id).find(filter);
  }

  @post('/section-pages/{id}/content-sections', {
    responses: {
      '200': {
        description: 'SectionPage model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(ContentSection)},
        },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof SectionPage.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContentSection, {
            title: 'NewContentSectionInSectionPage',
            exclude: ['id'],
            optional: ['sectionPageId'],
          }),
        },
      },
    })
    contentSection: Omit<ContentSection, 'id'>,
  ): Promise<ContentSection> {
    return this.sectionPageRepository
      .contentSections(id)
      .create(contentSection);
  }

  @patch('/section-pages/{id}/content-sections', {
    responses: {
      '200': {
        description: 'SectionPage.ContentSection PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContentSection, {partial: true}),
        },
      },
    })
    contentSection: Partial<ContentSection>,
    @param.query.object('where', getWhereSchemaFor(ContentSection))
    where?: Where<ContentSection>,
  ): Promise<Count> {
    return this.sectionPageRepository
      .contentSections(id)
      .patch(contentSection, where);
  }

  @del('/section-pages/{id}/content-sections', {
    responses: {
      '200': {
        description: 'SectionPage.ContentSection DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(ContentSection))
    where?: Where<ContentSection>,
  ): Promise<Count> {
    return this.sectionPageRepository.contentSections(id).delete(where);
  }
}

import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { DataContentString } from '../models'
import { DataContentStringRepository } from '../repositories'

@authenticate('jwt')
export class DataContentStringController {
  constructor(
    @repository(DataContentStringRepository)
    public dataContentStringRepository: DataContentStringRepository,
  ) {}

  @post('/data-content-strings', {
    responses: {
      '200': {
        description: 'DataContentString model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(DataContentString)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentString, {
            title: 'NewDataContentString',
            exclude: ['id'],
          }),
        },
      },
    })
    dataContentString: Omit<DataContentString, 'id'>,
  ): Promise<DataContentString> {
    return this.dataContentStringRepository.create(dataContentString);
  }

  @get('/data-content-strings/count', {
    responses: {
      '200': {
        description: 'DataContentString model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DataContentString) where?: Where<DataContentString>,
  ): Promise<Count> {
    return this.dataContentStringRepository.count(where);
  }

  @get('/data-content-strings', {
    responses: {
      '200': {
        description: 'Array of DataContentString model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DataContentString, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DataContentString) filter?: Filter<DataContentString>,
  ): Promise<DataContentString[]> {
    return this.dataContentStringRepository.find(filter);
  }

  @patch('/data-content-strings', {
    responses: {
      '200': {
        description: 'DataContentString PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentString, {partial: true}),
        },
      },
    })
    dataContentString: DataContentString,
    @param.where(DataContentString) where?: Where<DataContentString>,
  ): Promise<Count> {
    return this.dataContentStringRepository.updateAll(dataContentString, where);
  }

  @get('/data-content-strings/{id}', {
    responses: {
      '200': {
        description: 'DataContentString model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DataContentString, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(DataContentString, {exclude: 'where'})
    filter?: FilterExcludingWhere<DataContentString>,
  ): Promise<DataContentString> {
    return this.dataContentStringRepository.findById(id, filter);
  }

  @patch('/data-content-strings/{id}', {
    responses: {
      '204': {
        description: 'DataContentString PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DataContentString, {partial: true}),
        },
      },
    })
    dataContentString: DataContentString,
  ): Promise<void> {
    await this.dataContentStringRepository.updateById(id, dataContentString);
  }

  @put('/data-content-strings/{id}', {
    responses: {
      '204': {
        description: 'DataContentString PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() dataContentString: DataContentString,
  ): Promise<void> {
    await this.dataContentStringRepository.replaceById(id, dataContentString);
  }

  @del('/data-content-strings/{id}', {
    responses: {
      '204': {
        description: 'DataContentString DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.dataContentStringRepository.deleteById(id);
  }
}

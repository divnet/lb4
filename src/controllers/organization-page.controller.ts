import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, getWhereSchemaFor, param, patch, post, requestBody } from '@loopback/rest'

import { Organization, Page } from '../models'
import { OrganizationRepository } from '../repositories'

@authenticate('jwt')
export class OrganizationPageController {
  constructor(
    @repository(OrganizationRepository)
    protected organizationRepository: OrganizationRepository,
  ) {}

  @get('/organizations/{id}/pages', {
    responses: {
      '200': {
        description: 'Array of Organization has many Page',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Page)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Page>,
  ): Promise<Page[]> {
    return this.organizationRepository.pages(id).find(filter);
  }

  @post('/organizations/{id}/pages', {
    responses: {
      '200': {
        description: 'Organization model instance',
        content: {'application/json': {schema: getModelSchemaRef(Page)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Organization.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Page, {
            title: 'NewPageInOrganization',
            exclude: ['id'],
            optional: ['organizationId'],
          }),
        },
      },
    })
    page: Omit<Page, 'id'>,
  ): Promise<Page> {
    return this.organizationRepository.pages(id).create(page);
  }

  @patch('/organizations/{id}/pages', {
    responses: {
      '200': {
        description: 'Organization.Page PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Page, {partial: true}),
        },
      },
    })
    page: Partial<Page>,
    @param.query.object('where', getWhereSchemaFor(Page)) where?: Where<Page>,
  ): Promise<Count> {
    return this.organizationRepository.pages(id).patch(page, where);
  }

  @del('/organizations/{id}/pages', {
    responses: {
      '200': {
        description: 'Organization.Page DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Page)) where?: Where<Page>,
  ): Promise<Count> {
    return this.organizationRepository.pages(id).delete(where);
  }
}

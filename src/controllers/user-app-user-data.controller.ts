import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { AppUserData, User } from '../models'
import { UserRepository } from '../repositories'

export class UserAppUserDataController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  @get('/users/{id}/app-user-data', {
    responses: {
      '200': {
        description: 'AppUserData belonging to User',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(AppUserData)},
          },
        },
      },
    },
  })
  async getAppUserData(
    @param.path.string('id') id: typeof User.prototype.id,
  ): Promise<AppUserData> {
    return this.userRepository.appUserData(id);
  }
}

import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { ContentSection, SectionPage } from '../models'
import { ContentSectionRepository } from '../repositories'

@authenticate('jwt')
export class ContentSectionSectionPageController {
  constructor(
    @repository(ContentSectionRepository)
    public contentSectionRepository: ContentSectionRepository,
  ) {}

  @get('/content-sections/{id}/section-page', {
    responses: {
      '200': {
        description: 'SectionPage belonging to ContentSection',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(SectionPage)},
          },
        },
      },
    },
  })
  async getSectionPage(
    @param.path.string('id') id: typeof ContentSection.prototype.id,
  ): Promise<SectionPage> {
    return this.contentSectionRepository.sectionPage(id);
  }
}

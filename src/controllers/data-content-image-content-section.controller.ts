import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { ContentSection, DataContentFile } from '../models'
import { DataContentFileRepository } from '../repositories'

@authenticate('jwt')
export class DataContentFileContentSectionController {
  constructor(
    @repository(DataContentFileRepository)
    public dataContentFileRepository: DataContentFileRepository,
  ) {}

  @get('/data-content-file/{id}/content-section', {
    responses: {
      '200': {
        description: 'ContentSection belonging to DataContentFile',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(ContentSection)},
          },
        },
      },
    },
  })
  async getContentSection(
    @param.path.string('id') id: typeof DataContentFile.prototype.id,
  ): Promise<ContentSection> {
    return this.dataContentFileRepository.contentSection(id);
  }
}

import 'moment-timezone'

import { authenticate, TokenService } from '@loopback/authentication'
import { inject } from '@loopback/core'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'
import bcrypt from 'bcryptjs'
import moment from 'moment'

import { Credentials, PasswordHasherBindings, TokenServiceBindings, UserServiceBindings } from '../keys'
import { User } from '../models'
import { AppUserDataRepository, LogRepository, UserRepository, UserRoleRepository } from '../repositories'
import { MailerService } from '../services/email'
import { BcryptHasher } from '../services/hash.password.encrypt'
import { MyUserService } from '../services/user-service'
import { HOST, HOST_ADMIN, PASSWORD_RECOVERY_TEMPLATE, PASSWORD_SUCCESS_RECOVERY_TEMPLATE } from '../templates'

@authenticate('jwt')
export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(UserRoleRepository)
    public userRoleRepository: UserRoleRepository,
    @repository(LogRepository)
    public logRepository: LogRepository,
    @repository(AppUserDataRepository)
    public appUserDataRepository: AppUserDataRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public hasher: BcryptHasher,
  ) {}
  @post('/users/signup', {
    responses: {
      '200': {
        description: 'User Sign up',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  type: 'string',
                },
                email: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  @authenticate.skip()
  async createUser(@requestBody() user: User) {
    const {username} = user;
    const {email} = user;
    const emailValidator = await this.userRepository.findOne({
      where: {email: email},
    });
    const usernameValidator = await this.userRepository.findOne({
      where: {username: username},
    });
    if (usernameValidator?.username === username) {
      return {message: 'Username already exists'};
    }
    if (emailValidator?.email === email) {
      return {message: 'Email already exists'};
    }
    // eslint-disable-next-line require-atomic-updates
    const password = await this.hasher.hashPassword(user.password);
    user.password = password;
    const savedUser = await this.userRepository.create(user);
    const savedCredentials = await this.userRepository
      .userCredentials(savedUser.id)
      .create({password});
    console.log(savedCredentials);
    delete savedUser.password;
    return savedUser;
  }

  @get('/users/count', {
    responses: {
      '200': {
        description: 'User model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(User) where?: Where<User>): Promise<Count> {
    return this.userRepository.count(where);
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(User) filter?: Filter<User>): Promise<User[]> {
    const user = await this.userRepository.find(filter);
    user.forEach((u) => {
      delete u.password;
    });
    return user;
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'User PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    const password = await this.hasher.hashPassword(user.password);
    user.password = password;
    await this.userRepository.userCredentials(user.id).patch({password});
    return this.userRepository.updateAll(user, where);
  }

  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(User, {exclude: 'where'}) filter?: FilterExcludingWhere<User>,
  ): Promise<User> {
    const user = await this.userRepository.findById(id, filter);
    delete user.password;
    return user;
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
  ): Promise<void> {
    if (user.password) {
      try {
        const password = await this.hasher.hashPassword(user.password);
        user.password = password;
        await this.userRepository.userCredentials(id).patch({password});
        return await this.userRepository.updateById(id, user);
      } catch (err) {
        return err;
      }
    }
    return this.userRepository.updateById(id, user);
  }

  @put('/users/{id}', {
    responses: {
      '204': {
        description: 'User PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() user: User,
  ): Promise<void> {
    await this.userRepository.replaceById(id, user);
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.userRepository.deleteById(id);
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Generate a token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  @authenticate.skip()
  async login(@requestBody() credentials: Credentials) {
    try {
      // ensure the user exists, and the password is correct
      const user = await this.userService.verifyCredentials(credentials);
      // convert a User object into a UserProfile object (reduced set of properties)
      const userProfile = this.userService.convertToUserProfile(user);
      // create a JSON Web Token based on the user profile
      const token = await this.jwtService.generateToken(userProfile);
      const roles = await this.userRoleRepository.find({
        where: {userId: user.id},
      });
      const {id, email} = user;
      return {
        token,
        id: id as string,
        email,
        roles: roles.map((r) => r.roleId),
      };
    } catch (error) {
      return error;
    }
  }

  //////////////// RESET PASSWORD MAILS

  @post('/users/request-reset-password', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Reset User Password',
      },
    },
  })
  async sendResetPassword(
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              email: {type: 'string'},
            },
          },
        },
      },
    })
    user: User,
  ): Promise<object> {
    try {
      const emailValidator = await this.userRepository.findOne({
        where: {email: user.email},
      });

      if (emailValidator) {
        const hash = bcrypt.hashSync('}*/.,41-a[wRñ{1337}|', 8).toString();
        const token = Buffer.from(hash).toString('base64');
        const link_reset_password = HOST + '/resetpassword/' + token;

        let HTML_TEMPLATE = PASSWORD_RECOVERY_TEMPLATE;
        HTML_TEMPLATE = HTML_TEMPLATE.replace(
          '{{LINK_RESET_PASSWORD}}',
          link_reset_password,
        );
        const mailer = new MailerService();
        const options = {
          to: user.email,
          subject: 'Restablece tu contraseña',
          html: HTML_TEMPLATE,
        };

        const now = moment().format();
        if (
          moment(now).isAfter(emailValidator.token_expiration) ||
          emailValidator.token === '' ||
          emailValidator.token === undefined
        ) {
          this.userRepository.updateById(emailValidator.id, {
            token: token,
            token_expiration: moment().add(10, 'minutes').format('LLLL'),
          });
          mailer.sendMail(options);

          return {
            success: true,
            message:
              'se envio un enlace para restablecer tu contaseña al correo proporcionado',
          };
        } else {
          return {
            success: false,
            message:
              'ya existe un token para el usuario, porfavor espera 5 minutos para generar uno nuevo',
          };
        }
      } else {
        return {success: false};
      }
    } catch (error) {
      return error;
    }
  }

  @post('/users/reset-password', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Reset Admin Password',
      },
    },
  })
  async resetPassword(
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              password: {type: 'string'},
              token: {type: 'string'},
            },
          },
        },
      },
    })
    user: User,
  ): Promise<object> {
    // user reset password intent
    try {
      const userByToken = await this.userRepository.findOne({
        where: {token: user.token},
      });

      if (user && userByToken) {
        const now = moment().format();
        if (moment(userByToken.token_expiration).isAfter(now)) {
          // eslint-disable-next-line require-atomic-updates
          const password = await this.hasher.hashPassword(user.password);
          await this.userRepository.updateById(userByToken.id, {
            password: password,
            token: '',
            token_expiration: now,
          });

          const template = PASSWORD_SUCCESS_RECOVERY_TEMPLATE.replace(
            '{{URL_SUCCESS_RESET_PASSWORD}}',
            HOST,
          );
          const mailer = new MailerService();
          const options = {
            to: userByToken.email,
            subject: 'Contraseña restablecida',
            html: template,
          };
          mailer.sendMail(options);
          return {success: true};
        } else {
          return {
            success: false,
            message: 'porfavor verifica los datos, o el token ha expirado',
          };
        }
      } else {
        return {success: false, message: 'no existen datos relacionados'};
      }
    } catch (error) {
      return error;
    }
  }

  //---------
  @post('/users/admin/request-reset-password', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Admin Reset User Password',
      },
    },
  })
  async sendAdminResetPassword(
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              email: {type: 'string'},
            },
          },
        },
      },
    })
    user: User,
  ): Promise<object> {
    try {
      const emailValidator = await this.userRepository.findOne({
        where: {email: user.email},
      });

      if (emailValidator) {
        const hash = bcrypt.hashSync('|-.3,/*Ñ[a+]*}{41337', 8).toString();
        const token = Buffer.from(hash).toString('base64');
        const link_reset_password = HOST_ADMIN + '/resetpassword/' + token;
        const mailer = new MailerService();

        let HTML_TEMPLATE = PASSWORD_RECOVERY_TEMPLATE;
        HTML_TEMPLATE = HTML_TEMPLATE.replace(
          '{{LINK_RESET_PASSWORD}}',
          link_reset_password,
        );
        const options = {
          to: user.email,
          subject: 'Restablece tu contraseña',
          html: HTML_TEMPLATE,
        };

        const now = moment().format();
        if (
          moment(now).isAfter(emailValidator.token_expiration) ||
          emailValidator.token === '' ||
          emailValidator.token === undefined
        ) {
          this.userRepository.updateById(emailValidator.id, {
            token: token,
            token_expiration: moment().add(10, 'minutes').format('LLLL'),
          });
          mailer.sendMail(options);

          return {
            success: true,
            message:
              'se envio un enlace para restablecer tu contaseña al correo proporcionado',
          };
        } else {
          return {
            success: false,
            message:
              'ya existe un token para el usuario, porfavor espera 5 minutos para generar uno nuevo',
          };
        }
      } else {
        return {
          success: false,
          message: 'porfavor verifica que los datos sean correctos',
        };
      }
    } catch (error) {
      return error;
    }
  }

  @post('/users/admin/reset-admin-password', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Reset Admin Password',
      },
    },
  })
  async adminResetPassword(
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              password: {type: 'string'},
              token: {type: 'string'},
            },
          },
        },
      },
    })
    user: User,
  ): Promise<object> {
    try {
      const userByToken = await this.userRepository.findOne({
        where: {token: user.token},
      });

      if (user && userByToken) {
        const now = moment().format();
        if (moment(userByToken.token_expiration).isAfter(now)) {
          // eslint-disable-next-line require-atomic-updates
          const password = await this.hasher.hashPassword(user.password);
          await this.userRepository.updateById(userByToken.id, {
            password: password,
            token: '',
            token_expiration: now,
          });

          const template = PASSWORD_SUCCESS_RECOVERY_TEMPLATE.replace(
            '{{URL_SUCCESS_RESET_PASSWORD}}',
            HOST_ADMIN,
          );
          const mailer = new MailerService();
          const options = {
            to: userByToken.email,
            subject: 'Contraseña restablecida',
            html: template,
          };
          mailer.sendMail(options);
          return {success: true};
        } else {
          return {
            success: false,
            message: 'porfavor verifica los datos, o el token ha expirado',
          };
        }
      } else {
        return {success: false, message: 'no existen datos relacionados'};
      }
    } catch (error) {
      return error;
    }
  }
}

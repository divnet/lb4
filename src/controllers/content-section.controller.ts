import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { ContentSection } from '../models'
import { ContentSectionRepository } from '../repositories'

@authenticate('jwt')
export class ContentSectionController {
  constructor(
    @repository(ContentSectionRepository)
    public contentSectionRepository: ContentSectionRepository,
  ) {}

  @post('/content-sections', {
    responses: {
      '200': {
        description: 'ContentSection model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(ContentSection)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContentSection, {
            title: 'NewContentSection',
            exclude: ['id'],
          }),
        },
      },
    })
    contentSection: Omit<ContentSection, 'id'>,
  ): Promise<ContentSection> {
    return this.contentSectionRepository.create(contentSection);
  }

  @get('/content-sections/count', {
    responses: {
      '200': {
        description: 'ContentSection model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ContentSection) where?: Where<ContentSection>,
  ): Promise<Count> {
    return this.contentSectionRepository.count(where);
  }

  @get('/content-sections', {
    responses: {
      '200': {
        description: 'Array of ContentSection model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ContentSection, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ContentSection) filter?: Filter<ContentSection>,
  ): Promise<ContentSection[]> {
    return this.contentSectionRepository.find(filter);
  }

  @patch('/content-sections', {
    responses: {
      '200': {
        description: 'ContentSection PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContentSection, {partial: true}),
        },
      },
    })
    contentSection: ContentSection,
    @param.where(ContentSection) where?: Where<ContentSection>,
  ): Promise<Count> {
    return this.contentSectionRepository.updateAll(contentSection, where);
  }

  @get('/content-sections/{id}', {
    responses: {
      '200': {
        description: 'ContentSection model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ContentSection, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(ContentSection, {exclude: 'where'})
    filter?: FilterExcludingWhere<ContentSection>,
  ): Promise<ContentSection> {
    return this.contentSectionRepository.findById(id, filter);
  }

  @patch('/content-sections/{id}', {
    responses: {
      '204': {
        description: 'ContentSection PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContentSection, {partial: true}),
        },
      },
    })
    contentSection: ContentSection,
  ): Promise<void> {
    await this.contentSectionRepository.updateById(id, contentSection);
  }

  @put('/content-sections/{id}', {
    responses: {
      '204': {
        description: 'ContentSection PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() contentSection: ContentSection,
  ): Promise<void> {
    await this.contentSectionRepository.replaceById(id, contentSection);
  }

  @del('/content-sections/{id}', {
    responses: {
      '204': {
        description: 'ContentSection DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.contentSectionRepository.deleteById(id);
  }
}

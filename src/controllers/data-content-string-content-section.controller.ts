import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { ContentSection, DataContentString } from '../models'
import { DataContentStringRepository } from '../repositories'

@authenticate('jwt')
export class DataContentStringContentSectionController {
  constructor(
    @repository(DataContentStringRepository)
    public dataContentStringRepository: DataContentStringRepository,
  ) {}

  @get('/data-content-strings/{id}/content-section', {
    responses: {
      '200': {
        description: 'ContentSection belonging to DataContentString',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(ContentSection)},
          },
        },
      },
    },
  })
  async getContentSection(
    @param.path.string('id') id: typeof DataContentString.prototype.id,
  ): Promise<ContentSection> {
    return this.dataContentStringRepository.contentSection(id);
  }
}

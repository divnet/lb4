import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository'
import { del, get, getModelSchemaRef, param, patch, post, put, requestBody } from '@loopback/rest'

import { BlogCategory } from '../models'
import { BlogCategoryRepositoryRepository } from '../repositories'

@authenticate('jwt')
export class BlogCategoryController {
  constructor(
    @repository(BlogCategoryRepositoryRepository)
    public blogCategoryRepositoryRepository: BlogCategoryRepositoryRepository,
  ) {}

  @post('/blog-categories', {
    responses: {
      '200': {
        description: 'BlogCategory model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(BlogCategory)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BlogCategory, {
            title: 'NewBlogCategory',
            exclude: ['id'],
          }),
        },
      },
    })
    blogCategory: Omit<BlogCategory, 'id'>,
  ): Promise<BlogCategory> {
    return this.blogCategoryRepositoryRepository.create(blogCategory);
  }

  @get('/blog-categories/count', {
    responses: {
      '200': {
        description: 'BlogCategory model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(BlogCategory) where?: Where<BlogCategory>,
  ): Promise<Count> {
    return this.blogCategoryRepositoryRepository.count(where);
  }

  @get('/blog-categories', {
    responses: {
      '200': {
        description: 'Array of BlogCategory model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(BlogCategory, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(BlogCategory) filter?: Filter<BlogCategory>,
  ): Promise<BlogCategory[]> {
    return this.blogCategoryRepositoryRepository.find(filter);
  }

  @patch('/blog-categories', {
    responses: {
      '200': {
        description: 'BlogCategory PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BlogCategory, {partial: true}),
        },
      },
    })
    blogCategory: BlogCategory,
    @param.where(BlogCategory) where?: Where<BlogCategory>,
  ): Promise<Count> {
    return this.blogCategoryRepositoryRepository.updateAll(blogCategory, where);
  }

  @get('/blog-categories/{id}', {
    responses: {
      '200': {
        description: 'BlogCategory model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(BlogCategory, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(BlogCategory, {exclude: 'where'})
    filter?: FilterExcludingWhere<BlogCategory>,
  ): Promise<BlogCategory> {
    return this.blogCategoryRepositoryRepository.findById(id, filter);
  }

  @patch('/blog-categories/{id}', {
    responses: {
      '204': {
        description: 'BlogCategory PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(BlogCategory, {partial: true}),
        },
      },
    })
    blogCategory: BlogCategory,
  ): Promise<void> {
    await this.blogCategoryRepositoryRepository.updateById(id, blogCategory);
  }

  @put('/blog-categories/{id}', {
    responses: {
      '204': {
        description: 'BlogCategory PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() blogCategory: BlogCategory,
  ): Promise<void> {
    await this.blogCategoryRepositoryRepository.replaceById(id, blogCategory);
  }

  @del('/blog-categories/{id}', {
    responses: {
      '204': {
        description: 'BlogCategory DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.blogCategoryRepositoryRepository.deleteById(id);
  }
}

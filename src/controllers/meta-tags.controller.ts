import { authenticate } from '@loopback/authentication'
import { Count, CountSchema, Filter, repository, Where } from '@loopback/repository'
import {
  del,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest'

import { MetaTags } from '../models'
import { MetaTagsRepository } from '../repositories'

@authenticate('jwt')
export class MetaTagsController {
  constructor(
    @repository(MetaTagsRepository)
    public metaTagsRepository: MetaTagsRepository,
  ) {}

  @post('/meta-tags', {
    responses: {
      '200': {
        description: 'MetaTags model instance',
        content: {'application/json': {schema: getModelSchemaRef(MetaTags)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetaTags, {
            title: 'NewMetaTags',
            exclude: ['id'],
          }),
        },
      },
    })
    metaTags: Omit<MetaTags, 'id'>,
  ): Promise<MetaTags> {
    return this.metaTagsRepository.create(metaTags);
  }

  @get('/meta-tags/count', {
    responses: {
      '200': {
        description: 'MetaTags model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(MetaTags))
    where?: Where<MetaTags>,
  ): Promise<Count> {
    return this.metaTagsRepository.count(where);
  }

  @get('/meta-tags', {
    responses: {
      '200': {
        description: 'Array of MetaTags model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MetaTags, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(MetaTags))
    filter?: Filter<MetaTags>,
  ): Promise<MetaTags[]> {
    return this.metaTagsRepository.find(filter);
  }

  @patch('/meta-tags', {
    responses: {
      '200': {
        description: 'MetaTags PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetaTags, {partial: true}),
        },
      },
    })
    metaTags: MetaTags,
    @param.query.object('where', getWhereSchemaFor(MetaTags))
    where?: Where<MetaTags>,
  ): Promise<Count> {
    return this.metaTagsRepository.updateAll(metaTags, where);
  }

  @get('/meta-tags/{id}', {
    responses: {
      '200': {
        description: 'MetaTags model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MetaTags, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(MetaTags))
    filter?: Filter<MetaTags>,
  ): Promise<MetaTags> {
    return this.metaTagsRepository.findById(id, filter);
  }

  @patch('/meta-tags/{id}', {
    responses: {
      '204': {
        description: 'MetaTags PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetaTags, {partial: true}),
        },
      },
    })
    metaTags: MetaTags,
  ): Promise<void> {
    await this.metaTagsRepository.updateById(id, metaTags);
  }

  @put('/meta-tags/{id}', {
    responses: {
      '204': {
        description: 'MetaTags PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() metaTags: MetaTags,
  ): Promise<void> {
    await this.metaTagsRepository.replaceById(id, metaTags);
  }

  @del('/meta-tags/{id}', {
    responses: {
      '204': {
        description: 'MetaTags DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.metaTagsRepository.deleteById(id);
  }
}

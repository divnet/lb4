import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Organization,
  SysUserRole,
} from '../models';
import {OrganizationRepository} from '../repositories';

export class OrganizationSysUserRoleController {
  constructor(
    @repository(OrganizationRepository) protected organizationRepository: OrganizationRepository,
  ) { }

  @get('/organizations/{id}/sys-user-roles', {
    responses: {
      '200': {
        description: 'Array of Organization has many SysUserRole',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(SysUserRole)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<SysUserRole>,
  ): Promise<SysUserRole[]> {
    return this.organizationRepository.sysUserRoles(id).find(filter);
  }

  @post('/organizations/{id}/sys-user-roles', {
    responses: {
      '200': {
        description: 'Organization model instance',
        content: {'application/json': {schema: getModelSchemaRef(SysUserRole)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Organization.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserRole, {
            title: 'NewSysUserRoleInOrganization',
            exclude: ['id'],
            optional: ['organizationId']
          }),
        },
      },
    }) sysUserRole: Omit<SysUserRole, 'id'>,
  ): Promise<SysUserRole> {
    return this.organizationRepository.sysUserRoles(id).create(sysUserRole);
  }

  @patch('/organizations/{id}/sys-user-roles', {
    responses: {
      '200': {
        description: 'Organization.SysUserRole PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SysUserRole, {partial: true}),
        },
      },
    })
    sysUserRole: Partial<SysUserRole>,
    @param.query.object('where', getWhereSchemaFor(SysUserRole)) where?: Where<SysUserRole>,
  ): Promise<Count> {
    return this.organizationRepository.sysUserRoles(id).patch(sysUserRole, where);
  }

  @del('/organizations/{id}/sys-user-roles', {
    responses: {
      '200': {
        description: 'Organization.SysUserRole DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(SysUserRole)) where?: Where<SysUserRole>,
  ): Promise<Count> {
    return this.organizationRepository.sysUserRoles(id).delete(where);
  }
}

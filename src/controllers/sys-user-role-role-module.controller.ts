import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { RoleModule, SysUserRole } from '../models'
import { SysUserRoleRepository } from '../repositories'

@authenticate('jwt')
export class SysUserRoleRoleModuleController {
  constructor(
    @repository(SysUserRoleRepository)
    public sysUserRoleRepository: SysUserRoleRepository,
  ) {}

  @get('/sys-user-roles/{id}/role-module', {
    responses: {
      '200': {
        description: 'RoleModule belonging to SysUserRole',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(RoleModule)},
          },
        },
      },
    },
  })
  async getRoleModule(
    @param.path.string('id') id: typeof SysUserRole.prototype.id,
  ): Promise<RoleModule> {
    return this.sysUserRoleRepository.roleModule(id);
  }
}

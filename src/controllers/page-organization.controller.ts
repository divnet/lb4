import { authenticate } from '@loopback/authentication'
import { repository } from '@loopback/repository'
import { get, getModelSchemaRef, param } from '@loopback/rest'

import { Organization, Page } from '../models'
import { PageRepository } from '../repositories'

@authenticate('jwt')
export class PageOrganizationController {
  constructor(
    @repository(PageRepository)
    public pageRepository: PageRepository,
  ) {}

  @get('/pages/{id}/organization', {
    responses: {
      '200': {
        description: 'Organization belonging to Page',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Organization)},
          },
        },
      },
    },
  })
  async getOrganization(
    @param.path.string('id') id: typeof Page.prototype.id,
  ): Promise<Organization> {
    return this.pageRepository.organization(id);
  }
}

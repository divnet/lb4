import jwt = require('jsonwebtoken');
import { TokenService } from '@loopback/authentication'
import { UserProfile, securityId } from '@loopback/security';
import { HttpErrors } from '@loopback/rest';
import { inject } from '@loopback/core';
import { TokenServiceBindings } from '../keys';
const signAsync = jwt.sign;

export class JWTService {

  @inject(TokenServiceBindings.TOKEN_SECRET)
  private jwtSecret: string;
  @inject(TokenServiceBindings.TOKEN_EXPIRES_IN)
  private jwtExpiresIn: string;

  async generateToken(userProfile: UserProfile): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized(
        'Error while generating token : userprofile is null',
      );
    }
    let token: string;
    try {
      token = await signAsync(userProfile, this.jwtSecret, {
        expiresIn: this.jwtExpiresIn
      });
    } catch (err) {
      throw new HttpErrors.Unauthorized(`error generating token ${err}`);
    }
    return token;
  }
  async verifyToken(token: string): Promise<UserProfile> {

    // if (!token) {
    //   throw new HttpErrors.Unauthorized(
    //     `Error verifying token: 'token' is null`,
    //   )
    // }
    // let userProfile: UserProfile;
    // try {
    //   // decode user profile from token
    //   const decryptedToken = await verifyToken(token, this.jwtSecret);
    //   // don't copy over  token field 'iat' and 'exp', nor 'email' to user profile
    //   userProfile = Object.assign(
    //     { [securityId]: '', name: '' },
    //     { [securityId]: decryptedToken.id, name: decryptedToken.name },
    //   );
    // } catch (error) {
    //   throw new HttpErrors.Unauthorized(
    //     `Error verifying token: ${error.message}`,
    //   );
    // }
    // return userProfile;
    return Promise.resolve({ [securityId]: '', name: 'Haider' });
    // return Promise.resolve(userProfile);

  }
}

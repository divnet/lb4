import { UserService } from '@loopback/authentication'
import { inject } from '@loopback/core'
import { repository } from '@loopback/repository'
import { HttpErrors } from '@loopback/rest'
import { securityId, UserProfile } from '@loopback/security'

import { Credentials, PasswordHasherBindings } from '../keys'
import { User } from '../models/user.model'
import { UserRepository } from '../repositories'
import { BcryptHasher } from './hash.password.encrypt'

export class MyUserService implements UserService<User, Credentials> {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public hasher: BcryptHasher,
  ) {}
  async verifyCredentials(credentials: Credentials): Promise<User> {
    const foundUser = await this.userRepository.findOne({
      where: {
        or: [{email: credentials.username}, {username: credentials.username}],
      },
    });
    console.log(foundUser);
    if (!foundUser) {
      throw new HttpErrors.NotFound('User not found');
    }
    const passwordMatch = await this.hasher.comparePassword(
      credentials.password,
      foundUser.password,
    );

    if (!passwordMatch) {
      throw new HttpErrors.Unauthorized('password is not valid');
    }
    return foundUser;
  }
  convertToUserProfile(user: User): UserProfile {
    let userName = '';
    if (user.firstName) {
      userName = user.firstName;
    }
    if (user.lastName) {
      userName = user.firstName
        ? `${user.firstName} ${user.lastName} `
        : user.lastName;
    }
    if (userName === '' && user.username) {
      userName = user.username;
    }
    const userProfile: UserProfile = {
      [securityId]: `${user.id}`,
      email: user.email,
      name: userName,
    };
    // return { securityId: `${user.id}`,email: user.email, name: user.name };
    return userProfile;
  }
}

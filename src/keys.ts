import { TokenService, UserService } from '@loopback/authentication'
import { BindingKey } from '@loopback/core'

import { User } from './models'
import { PasswordHasher } from './services/hash.password.encrypt'
import { FileUploadHandler } from './types'

export interface Credentials {
  email: string;
  username: string;
  password: string;
}

export namespace TokenServiceConstants {
  export const TOKEN_SECRET_VALUE = '123assda8213';
  export const TOKEN_EXPIRES_IN_VALUE = '7h';
}

export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret',
  );
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
    'authentication.jwt.expiresIn',
  );
  export const TOKEN_SERVICE = BindingKey.create<TokenService>(
    'authentication.jwt.service',
  );
}
export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER = BindingKey.create<PasswordHasher>(
    'service.hasher',
  );
  export const ROUNDS = BindingKey.create<number>('services.hasher.rounds');
}

export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<UserService<User, Credentials>>(
    'services.user.service',
  );
  export const USER_REPOSITORY = 'repositories.UserRepository';
  export const USER_CREDENTIALS_REPOSITORY =
    'repositories.UserCredentialsRepository';
}

export const FILE_UPLOAD_SERVICE = BindingKey.create<FileUploadHandler>(
  'services.FileUpload',
);

export namespace EmailServiceBindings {
  export const EMAIL_SERVICE = BindingKey.create<object>('email.service');
}

export const STORAGE_DIRECTORY = BindingKey.create<string>('storage.directory');

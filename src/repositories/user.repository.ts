import { Getter, inject } from '@loopback/core'
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  HasManyRepositoryFactory,
  HasOneRepositoryFactory,
  repository,
} from '@loopback/repository'

import { DatabaseDataSource } from '../datasources'
import { AppUserData, Log, SysUserData, User, UserCredentials, UserRelations } from '../models'
import { AppUserDataRepository } from './app-user-data.repository'
import { LogRepository } from './log.repository'
import { SysUserDataRepository } from './sys-user-data.repository'
import { UserCredentialsRepository } from './user-credentials.repository'

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  public readonly userCredentials: HasOneRepositoryFactory<
    UserCredentials,
    typeof User.prototype.id
  >;

  public readonly sysUserData: BelongsToAccessor<
    SysUserData,
    typeof User.prototype.id
  >;

  public readonly appUserData: BelongsToAccessor<
    AppUserData,
    typeof User.prototype.id
  >;

  public readonly logs: HasManyRepositoryFactory<Log, typeof User.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
    @repository.getter('UserCredentialsRepository')
    protected userCredentialsRepositoryGetter: Getter<
      UserCredentialsRepository
    >,
    @repository.getter('SysUserDataRepository')
    protected sysUserDataRepositoryGetter: Getter<SysUserDataRepository>,
    @repository.getter('AppUserDataRepository')
    protected appUserDataRepositoryGetter: Getter<AppUserDataRepository>,
    @repository.getter('LogRepository')
    protected logRepositoryGetter: Getter<LogRepository>,
  ) {
    super(User, dataSource);
    this.userCredentials = this.createHasOneRepositoryFactoryFor(
      'userCredentials',
      userCredentialsRepositoryGetter,
    );
    this.registerInclusionResolver(
      'userCredentials',
      this.userCredentials.inclusionResolver,
    );
    this.logs = this.createHasManyRepositoryFactoryFor(
      'logs',
      logRepositoryGetter,
    );
    this.registerInclusionResolver('logs', this.logs.inclusionResolver);
    this.appUserData = this.createBelongsToAccessorFor(
      'appUserData',
      appUserDataRepositoryGetter,
    );
    this.registerInclusionResolver(
      'appUserData',
      this.appUserData.inclusionResolver,
    );
    this.sysUserData = this.createBelongsToAccessorFor(
      'sysUserData',
      sysUserDataRepositoryGetter,
    );
    this.registerInclusionResolver(
      'sysUserData',
      this.sysUserData.inclusionResolver,
    );
  }

  async findCredentials(
    userId: typeof User.prototype.id,
  ): Promise<UserCredentials | undefined> {
    try {
      return await this.userCredentials(userId).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }
}

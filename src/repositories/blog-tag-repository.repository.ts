import {DefaultCrudRepository} from '@loopback/repository';
import {BlogTag, BlogTagRelations} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class BlogTagRepositoryRepository extends DefaultCrudRepository<
  BlogTag,
  typeof BlogTag.prototype.id,
  BlogTagRelations
> {
  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
  ) {
    super(BlogTag, dataSource);
  }
}

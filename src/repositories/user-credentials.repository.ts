// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
import { inject } from '@loopback/core'
import { DefaultCrudRepository } from '@loopback/repository'

import { DatabaseDataSource } from '../datasources'
import { UserCredentials, UserCredentialsRelations } from '../models'

export class UserCredentialsRepository extends DefaultCrudRepository<
  UserCredentials,
  typeof UserCredentials.prototype.id,
  UserCredentialsRelations
> {
  constructor(@inject('datasources.database') dataSource: DatabaseDataSource) {
    super(UserCredentials, dataSource);
  }
}

import {DefaultCrudRepository} from '@loopback/repository';
import {BlogCategory, BlogCategoryRelations} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class BlogCategoryRepositoryRepository extends DefaultCrudRepository<
  BlogCategory,
  typeof BlogCategory.prototype.id,
  BlogCategoryRelations
> {
  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
  ) {
    super(BlogCategory, dataSource);
  }
}

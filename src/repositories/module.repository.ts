import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Module, ModuleRelations, RoleModule} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {RoleModuleRepository} from './role-module.repository';

export class ModuleRepository extends DefaultCrudRepository<
  Module,
  typeof Module.prototype.id,
  ModuleRelations
> {

  public readonly roleModules: HasManyRepositoryFactory<RoleModule, typeof Module.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource, @repository.getter('RoleModuleRepository') protected roleModuleRepositoryGetter: Getter<RoleModuleRepository>,
  ) {
    super(Module, dataSource);
    this.roleModules = this.createHasManyRepositoryFactoryFor('roleModules', roleModuleRepositoryGetter,);
    this.registerInclusionResolver('roleModules', this.roleModules.inclusionResolver);
  }
}

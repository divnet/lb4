import { inject } from '@loopback/core'
import { DefaultCrudRepository } from '@loopback/repository'

import { DatabaseDataSource } from '../datasources'
import { Role } from '../models'

export class RoleRepository extends DefaultCrudRepository<
  Role,
  typeof Role.prototype.id
> {
  constructor(@inject('datasources.database') dataSource: DatabaseDataSource) {
    super(Role, dataSource);
  }
}

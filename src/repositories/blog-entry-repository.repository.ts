import {DefaultCrudRepository} from '@loopback/repository';
import {BlogEntry, BlogEntryRelations} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class BlogEntryRepositoryRepository extends DefaultCrudRepository<
  BlogEntry,
  typeof BlogEntry.prototype.id,
  BlogEntryRelations
> {
  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
  ) {
    super(BlogEntry, dataSource);
  }
}

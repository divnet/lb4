import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {RoleModule, RoleModuleRelations, Module} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {ModuleRepository} from './module.repository';

export class RoleModuleRepository extends DefaultCrudRepository<
  RoleModule,
  typeof RoleModule.prototype.id,
  RoleModuleRelations
> {

  public readonly module: BelongsToAccessor<Module, typeof RoleModule.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource, @repository.getter('ModuleRepository') protected moduleRepositoryGetter: Getter<ModuleRepository>,
  ) {
    super(RoleModule, dataSource);
    this.module = this.createBelongsToAccessorFor('module', moduleRepositoryGetter,);
    this.registerInclusionResolver('module', this.module.inclusionResolver);
  }
}

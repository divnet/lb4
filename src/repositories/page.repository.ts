import { DefaultCrudRepository, repository, HasManyRepositoryFactory, BelongsToAccessor } from '@loopback/repository';
import { Page, PageRelations, SectionPage, Organization } from '../models';
import { DatabaseDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { SectionPageRepository } from './section-page.repository';
import { OrganizationRepository } from './organization.repository';

export class PageRepository extends DefaultCrudRepository<
  Page,
  typeof Page.prototype.id,
  PageRelations
  > {

  public readonly sectionPages: HasManyRepositoryFactory<SectionPage, typeof Page.prototype.id>;

  public readonly organization: BelongsToAccessor<Organization, typeof Page.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
    @repository.getter('SectionPageRepository')
    protected sectionPageRepositoryGetter: Getter<SectionPageRepository>,
    @repository.getter('OrganizationRepository')
    protected organizationRepositoryGetter: Getter<OrganizationRepository>,
  ) {
    super(Page, dataSource);
    this.organization = this.createBelongsToAccessorFor('organization', organizationRepositoryGetter);
    this.registerInclusionResolver('organization', this.organization.inclusionResolver);
    this.sectionPages =
      this.createHasManyRepositoryFactoryFor('sectionPages', sectionPageRepositoryGetter);
    this.registerInclusionResolver('sectionPages', this.sectionPages.inclusionResolver);
  }
}

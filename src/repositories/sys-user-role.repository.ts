import { DefaultCrudRepository, repository, BelongsToAccessor, HasOneRepositoryFactory} from '@loopback/repository';
import { SysUserRole, User, RoleModule, SysUserData} from '../models';
import { DatabaseDataSource } from '../datasources';
import { inject, Getter} from '@loopback/core';
import {UserRepository} from './user.repository';
import {RoleModuleRepository} from './role-module.repository';
import {SysUserDataRepository} from './sys-user-data.repository';

export class SysUserRoleRepository extends DefaultCrudRepository<SysUserRole, typeof SysUserRole.prototype.id> {

  public readonly user: BelongsToAccessor<User, typeof SysUserRole.prototype.id>;

  public readonly roleModule: BelongsToAccessor<RoleModule, typeof SysUserRole.prototype.id>;

  public readonly sysUserData: HasOneRepositoryFactory<SysUserData, typeof SysUserRole.prototype.id>;

  constructor(@inject('datasources.database') dataSource: DatabaseDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>, @repository.getter('RoleModuleRepository') protected roleModuleRepositoryGetter: Getter<RoleModuleRepository>, @repository.getter('SysUserDataRepository') protected sysUserDataRepositoryGetter: Getter<SysUserDataRepository>,) {
    super(SysUserRole, dataSource);
    this.sysUserData = this.createHasOneRepositoryFactoryFor('sysUserData', sysUserDataRepositoryGetter);
    this.registerInclusionResolver('sysUserData', this.sysUserData.inclusionResolver);
    this.roleModule = this.createBelongsToAccessorFor('roleModule', roleModuleRepositoryGetter,);
    this.registerInclusionResolver('roleModule', this.roleModule.inclusionResolver);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}

import {DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory} from '@loopback/repository';
import {SectionPage, SectionPageRelations, Page, ContentSection} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {PageRepository} from './page.repository';
import {ContentSectionRepository} from './content-section.repository';

export class SectionPageRepository extends DefaultCrudRepository<
  SectionPage,
  typeof SectionPage.prototype.id,
  SectionPageRelations
> {

  public readonly page: BelongsToAccessor<Page, typeof SectionPage.prototype.id>;

  public readonly contentSections: HasManyRepositoryFactory<ContentSection, typeof SectionPage.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource, @repository.getter('PageRepository') protected pageRepositoryGetter: Getter<PageRepository>, @repository.getter('ContentSectionRepository') protected contentSectionRepositoryGetter: Getter<ContentSectionRepository>,
  ) {
    super(SectionPage, dataSource);
    this.contentSections = this.createHasManyRepositoryFactoryFor('contentSections', contentSectionRepositoryGetter,);
    this.registerInclusionResolver('contentSections', this.contentSections.inclusionResolver);
    this.page = this.createBelongsToAccessorFor('page', pageRepositoryGetter,);
    this.registerInclusionResolver('page', this.page.inclusionResolver);
  }
}

import {DefaultCrudRepository} from '@loopback/repository';
import {LeadContent, LeadContentRelations} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LeadContentRepository extends DefaultCrudRepository<
  LeadContent,
  typeof LeadContent.prototype.id,
  LeadContentRelations
> {
  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
  ) {
    super(LeadContent, dataSource);
  }
}

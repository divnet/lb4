import { DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory } from '@loopback/repository';
import { DataContentString, DataContentStringRelations, ContentSection } from '../models';
import { DatabaseDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { ContentSectionRepository } from './content-section.repository';

export class DataContentStringRepository extends DefaultCrudRepository<
  DataContentString,
  typeof DataContentString.prototype.id,
  DataContentStringRelations
  > {

  public readonly contentSection: BelongsToAccessor<ContentSection, typeof DataContentString.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
    @repository.getter('ContentSectionRepository') protected contentSectionRepositoryGetter: Getter<ContentSectionRepository>,
  ) {
    super(DataContentString, dataSource);
    this.contentSection = this.createBelongsToAccessorFor('contentSection', contentSectionRepositoryGetter);
    this.registerInclusionResolver('contentSection', this.contentSection.inclusionResolver);
  }
}

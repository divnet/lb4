import { DefaultCrudRepository, repository, BelongsToAccessor } from '@loopback/repository';
import { DataContentFile, DataContentFileRelations, ContentSection } from '../models';
import { DatabaseDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { ContentSectionRepository } from './content-section.repository';

export class DataContentFileRepository extends DefaultCrudRepository<
  DataContentFile,
  typeof DataContentFile.prototype.id,
  DataContentFileRelations
  > {

  public readonly contentSection: BelongsToAccessor<ContentSection, typeof DataContentFile.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource, @repository.getter('ContentSectionRepository') protected contentSectionRepositoryGetter: Getter<ContentSectionRepository>,
  ) {
    super(DataContentFile, dataSource);
    this.contentSection = this.createBelongsToAccessorFor('contentSection', contentSectionRepositoryGetter);
    this.registerInclusionResolver('contentSection', this.contentSection.inclusionResolver);
  }
}

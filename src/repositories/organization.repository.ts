import { DefaultCrudRepository, juggler, HasManyRepositoryFactory, BelongsToAccessor, repository } from '@loopback/repository';
import { Organization, Page, User, SysUserRole } from '../models';
import { DatabaseDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { PageRepository } from './page.repository';
import { UserRepository } from './user.repository';
import { SysUserRoleRepository } from './sys-user-role.repository';

export class OrganizationRepository extends DefaultCrudRepository<
  Organization,
  typeof Organization.prototype.id
  > {
  //  public readonly parent: BelongsToAccessor<Organization, string>;

  public readonly pages: HasManyRepositoryFactory<Page, typeof Organization.prototype.id>;

  public readonly users: HasManyRepositoryFactory<User, typeof Organization.prototype.id>;

  public readonly sysUserRoles: HasManyRepositoryFactory<SysUserRole, typeof Organization.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
    @repository.getter('PageRepository')
    protected pageRepositoryGetter: Getter<PageRepository>,

    @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,

    @repository.getter('SysUserRoleRepository') protected sysUserRoleRepositoryGetter: Getter<SysUserRoleRepository>,
  ) {
    super(Organization, dataSource);
    this.sysUserRoles = this.createHasManyRepositoryFactoryFor('sysUserRoles', sysUserRoleRepositoryGetter);
    this.registerInclusionResolver('sysUserRoles', this.sysUserRoles.inclusionResolver);
    this.users = this.createHasManyRepositoryFactoryFor('users', userRepositoryGetter);
    this.registerInclusionResolver('users', this.users.inclusionResolver);
    this.pages = this.createHasManyRepositoryFactoryFor('pages', pageRepositoryGetter);
    this.registerInclusionResolver('pages', this.pages.inclusionResolver);
    //this.parent = this._createBelongsToAccessorFor('parent', Getter.fromValue(this))
  }
}

import { DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory } from '@loopback/repository';
import { ContentSection, ContentSectionRelations, SectionPage, DataContentString, DataContentFile } from '../models';
import { DatabaseDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { SectionPageRepository } from './section-page.repository';
import { DataContentStringRepository } from './data-content-string.repository';
import { DataContentFileRepository } from './data-content-image.repository';

export class ContentSectionRepository extends DefaultCrudRepository<
  ContentSection,
  typeof ContentSection.prototype.id,
  ContentSectionRelations
  > {

  public readonly sectionPage: BelongsToAccessor<SectionPage, typeof ContentSection.prototype.id>;

  public readonly dataContentStrings: HasManyRepositoryFactory<DataContentString, typeof ContentSection.prototype.id>;

  public readonly dataContentFiles: HasManyRepositoryFactory<DataContentFile, typeof ContentSection.prototype.id>;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource, @repository.getter('SectionPageRepository') protected sectionPageRepositoryGetter: Getter<SectionPageRepository>, @repository.getter('DataContentStringRepository') protected dataContentStringRepositoryGetter: Getter<DataContentStringRepository>, @repository.getter('DataContentFileRepository') protected dataContentFileRepositoryGetter: Getter<DataContentFileRepository>,
  ) {
    super(ContentSection, dataSource);
    this.dataContentFiles = this.createHasManyRepositoryFactoryFor('dataContentFiles', dataContentFileRepositoryGetter);
    this.registerInclusionResolver('dataContentFiles', this.dataContentFiles.inclusionResolver);
    this.dataContentStrings = this.createHasManyRepositoryFactoryFor('dataContentStrings', dataContentStringRepositoryGetter);
    this.registerInclusionResolver('dataContentStrings', this.dataContentStrings.inclusionResolver);
    this.sectionPage = this.createBelongsToAccessorFor('sectionPage', sectionPageRepositoryGetter);
    this.registerInclusionResolver('sectionPage', this.sectionPage.inclusionResolver);
  }
}

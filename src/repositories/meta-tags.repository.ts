import {DefaultCrudRepository} from '@loopback/repository';
import {MetaTags, MetaTagsRelations} from '../models';
import {DatabaseDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class MetaTagsRepository extends DefaultCrudRepository<
  MetaTags,
  typeof MetaTags.prototype.id,
  MetaTagsRelations
> {
  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
  ) {
    super(MetaTags, dataSource);
  }
}

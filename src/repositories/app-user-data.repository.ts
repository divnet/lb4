import { Getter, inject } from '@loopback/core'
import { BelongsToAccessor, DefaultCrudRepository, repository } from '@loopback/repository'

import { DatabaseDataSource } from '../datasources'
import { AppUserData, User } from '../models'
import { UserRepository } from './user.repository'

export class AppUserDataRepository extends DefaultCrudRepository<
  AppUserData,
  typeof AppUserData.prototype.id
> {
  public readonly user: BelongsToAccessor<
    User,
    typeof AppUserData.prototype.id
  >;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(AppUserData, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}

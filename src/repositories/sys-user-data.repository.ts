import { Getter, inject } from '@loopback/core'
import { BelongsToAccessor, DefaultCrudRepository, repository } from '@loopback/repository'

import { DatabaseDataSource } from '../datasources'
import { SysUserData, SysUserRole, User } from '../models'
import { SysUserRoleRepository } from './sys-user-role.repository'
import { UserRepository } from './user.repository'

export class SysUserDataRepository extends DefaultCrudRepository<
  SysUserData,
  typeof SysUserData.prototype.id
> {
  public readonly user: BelongsToAccessor<
    User,
    typeof SysUserData.prototype.id
  >;

  public readonly sysUserRole: BelongsToAccessor<
    SysUserRole,
    typeof SysUserData.prototype.id
  >;

  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('SysUserRoleRepository')
    protected sysUserRoleRepositoryGetter: Getter<SysUserRoleRepository>,
  ) {
    super(SysUserData, dataSource);
    this.sysUserRole = this.createBelongsToAccessorFor(
      'sysUserRole',
      sysUserRoleRepositoryGetter,
    );
    this.registerInclusionResolver(
      'sysUserRole',
      this.sysUserRole.inclusionResolver,
    );
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
